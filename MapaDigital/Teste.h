#ifndef Teste_
#define Teste_

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include "Local.h"
#include "Natural.h"
#include "HistoricoCultural.h"
#include "ViaLigacao.h"
#include "AE.h"
#include "EN.h"

class Teste
{
private:
	int dimLoc, actLoc, dimVia, actVia;
	Local* *vecLocais;
	ViaLigacao* *vecVias;

public:
	Teste(int dimensaoLoc=5, int dimensaoVia=5);
	Teste(const Teste& t);
	~Teste();

	int getActLoc() const;
	int getActVia() const;

	Local** getVecLocais() const;
	ViaLigacao** getVecVias() const;

	int lerFxLocais(string s);
	int lerFxVias(string s);

	Local* encontrarLocal(string s);
	ViaLigacao* encontrarVia(string orig, string dest, string codVia);

	void inserirLocal(Local* loc);
	void inserirVia(ViaLigacao* via);

	void listarLocais() const;
	void listarVias() const;

	void contabilizarLocais() const;
	void ordenarLocais();

	Teste& operator=(const Teste& t);

	void escrever(ostream& out) const;
};

Teste::Teste(int dimensaoLoc, int dimensaoVia)
{
	dimLoc = dimensaoLoc;
	actLoc = 0;
	vecLocais = new Local* [dimLoc];

	dimVia = dimensaoVia;
	actVia = 0;
	vecVias = new ViaLigacao* [dimVia];
}

Teste::Teste(const Teste& t)
{
	dimLoc = t.dimLoc;
	actLoc = t.actLoc;
	vecLocais = new Local* [dimLoc];
	for (int i = 0; i < actLoc; i++)
		vecLocais[i]=t.vecLocais[i]->clone();

	dimVia = t.dimVia;
	actVia = t.actVia;
	vecVias = new ViaLigacao* [dimVia];
	for (int i = 0; i < actVia; i++)
		vecVias[i]=t.vecVias[i]->clone();
}

Teste::~Teste()
{
	for (int i = 0; i < actLoc; i++)
		delete vecLocais[i];
	delete []vecLocais;

	for (int i = 0; i < actVia; i++)
		delete vecVias[i];
	delete []vecVias;
}

int Teste::getActLoc() const
{
	return actLoc;
}

int Teste::getActVia() const
{
	return actVia;
}

Local** Teste::getVecLocais() const
{
	return vecLocais;
}

ViaLigacao** Teste::getVecVias() const
{
	return vecVias;
}

void Teste::inserirLocal(Local* loc)
{
	if(actLoc==dimLoc) // vector cheio
	{
		Local* *tmp = new Local* [dimLoc*2];
		for(int i=0; i<actLoc; i++)
			tmp[i]=vecLocais[i];
		delete []vecLocais;
		vecLocais=tmp;
		dimLoc=dimLoc*2;
	}

	vecLocais[actLoc]=loc->clone();
	actLoc++;
}

void Teste::inserirVia(ViaLigacao* via)
{
	if(actVia==dimVia) // vector cheio
	{
		ViaLigacao* *tmp = new ViaLigacao* [dimVia*2];
		for(int i=0; i<actVia; i++)
			tmp[i]=vecVias[i];
		delete []vecVias;
		vecVias=tmp;
		dimVia=dimVia*2;
	}

	vecVias[actVia]=via->clone();
	actVia++;
}

void Teste::listarLocais() const
{
	for(int i=0; i<actLoc; i++)
		vecLocais[i]->listar();
}

void Teste::listarVias() const
{
	for(int i=0; i<actVia; i++)
		vecVias[i]->listar();
}

void Teste::contabilizarLocais() const
{
	int histCult=0, nat=0;

	for(int i=0; i<actLoc; i++)
	{
		if (typeid(*vecLocais[i]) == typeid(HistoricoCultural))
			histCult++;
		if (typeid(*vecLocais[i]) == typeid(Natural))
			nat++;
	}

	cout << "Locais de Interesse Turistico Historico Cultural: " << histCult << endl;
	cout << "Locais de Interesse Turistico Natural: " << nat << endl;
}

void Teste::ordenarLocais()
{
	Local* l;

	for(int i=0; i<actLoc; i++)
	{
		for(int j=0; j<actLoc; j++)
		{
			if((*vecLocais[i])<(*vecLocais[j]))
			{
				l=vecLocais[i];
				vecLocais[i]=vecLocais[j];
				vecLocais[j]=l;
			}
		}
	}
}

Teste& Teste::operator=(const Teste& t)
{
	if(this!=&t)
	{
		for(int i=0; i<actLoc; i++)
			delete vecLocais[i];

		delete []vecLocais;
		dimLoc=t.dimLoc;
		actLoc=t.actLoc;
		vecLocais=new Local*[dimLoc];

		for(int i=0; i<actLoc; i++)
			vecLocais[i]=t.vecLocais[i]->clone();

		for(int i=0; i<actVia; i++)
			delete vecVias[i];

		delete []vecVias;
		dimVia=t.dimVia;
		actVia=t.actVia;
		vecVias=new ViaLigacao*[dimVia];

		for(int i=0; i<actVia; i++)
			vecVias[i]=t.vecVias[i]->clone();
	}
	return (*this);
}

void Teste::escrever(ostream& out) const
{
	for(int i=0; i<actLoc; i++)
		out<<(*vecLocais[i])<<endl;

	for(int i=0; i<actVia; i++)
		out<<(*vecVias[i])<<endl;
}

ostream& operator<<(ostream& o, const Teste& t)
{
	t.escrever(o);
	return o;
}

Local* Teste::encontrarLocal(string s)
{
	for(int i=0; i<actLoc; i++)
		if((*vecLocais[i]).getDesc() == s)
			return vecLocais[i];
	
	return NULL;
}

ViaLigacao* Teste::encontrarVia(string orig, string dest, string codVia)
{
	for(int i=0; i<actVia; i++)
		if((((*(*vecVias[i]).getOrigem()).getDesc()) == orig) && (((*(*vecVias[i]).getDestino()).getDesc()) == dest) && (((*vecVias[i]).getCodVia()) == codVia))
			return vecVias[i];

	return NULL;
}

int Teste::lerFxLocais(string s)
{
	ifstream fx;
	string linha;
	string partes[4];
	int inic=0;
	int contador=0;

	fx.open(s+".txt");

	if(!fx)
		return -1;
	
	while(!fx.eof())
	{
		int pos=0;
		int i=0;
		getline(fx,linha,'\n');
		if (linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			while (pos > 0)
			{
				pos = linha.find(',', inic);
				partes[i] = (linha.substr(inic,pos-inic));
				pos++;
				inic = pos;
				i++;
			}

			transform((partes[0]).begin(), (partes[0]).end(), (partes[0]).begin(), toupper);

			if(encontrarLocal(partes[0])==NULL)
			{
				if(i == 2)
				{
					Natural ln(partes[0], stoi(partes[1]));
					(*this).inserirLocal(&ln);
					contador++;
				}
				else if(i == 4)
				{
					HistoricoCultural lhc(partes[0], stoi(partes[1]), stoi(partes[2]), stoi(partes[3]));
					(*this).inserirLocal(&lhc);
					contador++;
				}
			}
		}
	}
	
	fx.close();
	return contador;
}

int Teste::lerFxVias(string s)
{
	ifstream fx;
	string linha;
	string partes[6];
	int inic=0;
	int contador=0;

	fx.open(s+".txt");
	if(!fx)
		return -1;
	
	while(!fx.eof())
	{
		int pos=0;
		int i=0;
		getline(fx,linha,'\n');
		if (linha.size() > 0)
		{
			int pos = linha.find(',', inic);
			while (pos > 0)
			{
				pos = linha.find(',', inic);
				partes[i] = (linha.substr(inic,pos-inic));
				pos++;
				inic = pos;
				i++;
			}

			transform((partes[0]).begin(), (partes[0]).end(), (partes[0]).begin(), toupper);
			transform((partes[1]).begin(), (partes[1]).end(), (partes[1]).begin(), toupper);
			transform((partes[2]).begin(), (partes[2]).end(), (partes[2]).begin(), toupper);

			if(encontrarLocal(partes[0])!=NULL && encontrarLocal(partes[1])!=NULL)
			{
				if(encontrarVia(partes[0], partes[1], partes[2])==NULL)
				{
					regex nacional("EN[1-9][[:digit:]]{0,2}(-[1-9])?");
					regex autoestrada("A[1-9][[:digit:]]?");

					if(regex_match(partes[2],nacional))
					{
						transform((partes[5]).begin(), (partes[5]).end(), (partes[5]).begin(), toupper);
						EN estNac(encontrarLocal(partes[0]),encontrarLocal(partes[1]), partes[2], stoi(partes[3]), stoi(partes[4]), partes[5]);
						(*this).inserirVia(&estNac);
						contador++;
					}
					else if(regex_match(partes[2],autoestrada))
					{
						AE autEst(encontrarLocal(partes[0]), encontrarLocal(partes[1]), partes[2], stoi(partes[3]), stoi(partes[4]), stof(partes[5]));
						(*this).inserirVia(&autEst);
						contador++;
					}
				}
			}
		}
	}
	
	fx.close();
	return contador;
}

#endif
