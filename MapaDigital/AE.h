#ifndef AE_
#define AE_

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

#include "ViaLigacao.h"
#include "Local.h"

class AE : public ViaLigacao
{
private:
	float portagem;

public:
	AE();
	AE(Local* orig, Local* dest, string codigoVia, int totKms, int tempoMedPerc, float port);
	AE(const AE& a);
	ViaLigacao* clone() const;
	~AE();

	float getPortagem() const;
	void setPortagem(float port);

	void listar() const;

	AE& operator=(const AE& a);

	void escrever(ostream& out) const;
};

AE::AE():ViaLigacao()
{
	portagem = 0;
}

AE::AE(Local* orig, Local* dest, string codigoVia, int totKms, int tempoMedPerc, float port):ViaLigacao(orig, dest, codigoVia, totKms, tempoMedPerc)
{
	portagem = port;
}

AE::AE(const AE& a):ViaLigacao(a)
{
	portagem = a.portagem;
}

ViaLigacao* AE::clone() const
{
	return new AE(*this);
}

AE::~AE()
{

}

float AE::getPortagem() const 
{
	return portagem;
}

void AE::setPortagem(float port) 
{
	portagem = port;
}

void AE::listar() const
{
	cout << "VIA DE LIGACAO - AUTO-ESTRADA" << endl;
	ViaLigacao::listar();
	cout << "PRECO PORTAGENS: " << fixed << setprecision(2) << portagem << " EUR" << endl;
	cout << endl;
}

AE& AE::operator=(const AE& a)
{
	if(this!=&a)
	{
		portagem=a.portagem;
		ViaLigacao::operator=(a);
	}
	return (*this);
}

void AE::escrever(ostream& out) const
{
	out << "VIA DE LIGACAO - AUTO-ESTRADA" << endl;
	ViaLigacao::escrever(cout);
	out << "PRECO PORTAGENS: " << fixed << setprecision(2) << portagem << " EUR" << endl;
}

#endif
