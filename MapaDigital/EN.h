#ifndef EN_
#define EN_

#include <iostream>
#include <string>

using namespace std;

#include "ViaLigacao.h"
#include "Local.h"

class EN : public ViaLigacao
{
private:
	string pavimento;

public:
	EN();
	EN(Local* orig, Local* dest, string codigoVia, int totKms, int tempoMedPerc, string pav);
	EN(const EN& e);
	ViaLigacao* clone() const;
	~EN();

	string getPavimento() const;
	void setPavimento(string pav);

	void listar() const;

	EN& operator=(const EN& e);

	void escrever(ostream& out) const;
};

EN::EN():ViaLigacao()
{
	pavimento = "";
}

EN::EN(Local* orig, Local* dest, string codigoVia, int totKms, int tempoMedPerc, string pav):ViaLigacao(orig, dest, codigoVia, totKms, tempoMedPerc)
{
	pavimento = pav;
}

EN::EN(const EN& e):ViaLigacao(e)
{
	pavimento = e.pavimento;
}

ViaLigacao* EN::clone() const
{
	return new EN(*this);
}

EN::~EN()
{

}

string EN::getPavimento() const 
{
	return pavimento;
}

void EN::setPavimento(string pav) 
{
	pavimento = pav;
}

void EN::listar() const
{
	cout << "VIA DE LIGACAO - ESTRADA NACIONAL" << endl;
	ViaLigacao::listar();
	cout << "PAVIMENTO: " << pavimento << endl;
	cout << endl;
}

EN& EN::operator=(const EN& e)
{
	if(this!=&e)
	{
		pavimento=e.pavimento;
		ViaLigacao::operator=(e);
	}
	return (*this);
}

void EN::escrever(ostream& out) const
{
	out << "VIA DE LIGACAO - ESTRADA NACIONAL" << endl;
	ViaLigacao::escrever(cout);
	out << "PAVIMENTO: " << pavimento << endl;
}

#endif
