#ifndef ViaLigacao_
#define ViaLigacao_

#include <iostream>
#include <string>

using namespace std;

#include "Local.h"

class ViaLigacao
{
private:
	Local* origem;
	Local* destino;
	string codVia;
	int totalKms;
	int tmpMedPerc;

public:
	ViaLigacao();
	ViaLigacao(Local* orig, Local* dest, string codigoVia, int totKms, int tempoMedPerc);
	ViaLigacao(const ViaLigacao& vl);
	virtual ViaLigacao* clone() const;
	virtual ~ViaLigacao();

	Local* getOrigem() const;
	Local* getDestino() const;
	string getCodVia() const;
	int getTotalKms() const;
	int getTmpMedPerc() const;
	void setOrigem(Local* orig);
	void setDestino(Local* dest);
	void setCodVia(string codigoVia);
	void setTotalKms(int totKms);
	void setTmpMedPerc(int tempoMedPerc);

	virtual void listar() const;

	ViaLigacao& operator=(const ViaLigacao& vl);

	bool operator==(const ViaLigacao& vl) const;
	bool operator<(const ViaLigacao& vl) const;
	bool operator>(const ViaLigacao& vl) const;

	virtual void escrever(ostream& out) const;
};

ViaLigacao::ViaLigacao()
{
	origem = NULL;
	destino = NULL;
	codVia = "";
	totalKms = 0;
	tmpMedPerc = 0;
}

ViaLigacao::ViaLigacao(Local* orig, Local* dest, string codigoVia, int totKms, int tempoMedPerc)
{
	origem = orig;
	destino = dest;
	codVia = codigoVia;
	totalKms = totKms;
	tmpMedPerc = tempoMedPerc;
}

ViaLigacao::ViaLigacao(const ViaLigacao& vl)
{
	origem = vl.origem;
	destino = vl.destino;
	codVia = vl.codVia;
	totalKms = vl.totalKms;
	tmpMedPerc = vl.tmpMedPerc;
}

ViaLigacao* ViaLigacao::clone() const
{
	return new ViaLigacao(*this);
}

ViaLigacao::~ViaLigacao()
{

}

Local* ViaLigacao::getOrigem() const 
{
	return origem;
}

Local* ViaLigacao::getDestino() const 
{
	return destino;
}

string ViaLigacao::getCodVia() const 
{
	return codVia;
}

int ViaLigacao::getTotalKms() const 
{
	return totalKms;
}

int ViaLigacao::getTmpMedPerc() const 
{
	return tmpMedPerc;
}

void ViaLigacao::setOrigem(Local* orig) 
{
	origem = orig;
}

void ViaLigacao::setDestino(Local* dest) 
{
	destino = dest;
}

void ViaLigacao::setCodVia(string codigoVia) 
{
	codVia = codigoVia;
}

void ViaLigacao::setTotalKms(int totKms) 
{
	totalKms = totKms;
}

void ViaLigacao::setTmpMedPerc(int tempoMedPerc) 
{
	tmpMedPerc = tempoMedPerc;
}

void ViaLigacao::listar() const
{
	cout << "ORIGEM: " << origem->getDesc() << endl;
	cout << "DESTINO: " << destino->getDesc() << endl;
	cout << "CODIGO VIA: " << codVia << endl;
	cout << "TOTAL KMS: " << totalKms << endl;
	cout << "TEMPO MEDIO PERCURSO: ";
	if(tmpMedPerc<10)
		cout << "00:0" << tmpMedPerc << "h" << endl;
	else
	{
		if(tmpMedPerc>=10 && tmpMedPerc<60)
			cout << "00:" << tmpMedPerc << "h" << endl;
		else
		{
			if((tmpMedPerc/60)<10 && (tmpMedPerc%60)<10)
				cout << "0" << tmpMedPerc/60 << ":0" << tmpMedPerc%60 << "h" << endl;
			if((tmpMedPerc/60)<10 && (tmpMedPerc%60)>=10)
				cout << "0" << tmpMedPerc/60 << ":" << tmpMedPerc%60 << "h" << endl;
			if((tmpMedPerc/60)>=10 && (tmpMedPerc%60)>=10)
				cout << tmpMedPerc/60 << ":" << tmpMedPerc%60 << "h" << endl;
			if((tmpMedPerc/60)>=10 && (tmpMedPerc%60)<10)
				cout << tmpMedPerc/60 << ":0" << tmpMedPerc%60 << "h" << endl;
		}
	}
}

ViaLigacao& ViaLigacao::operator=(const ViaLigacao& vl)
{
	if(this!=&vl)
	{
		origem = vl.origem;
		destino = vl.destino;
		codVia = vl.codVia;
		totalKms = vl.totalKms;
		tmpMedPerc = vl.tmpMedPerc;
	}
	return (*this);
}

bool ViaLigacao::operator==(const ViaLigacao& vl) const
{
	return ((origem == vl.origem) && (destino == vl.destino) && (codVia == vl.codVia) && (totalKms == vl.totalKms) && (tmpMedPerc == vl.tmpMedPerc));
}

bool ViaLigacao::operator<(const ViaLigacao& vl) const
{
	if(totalKms < vl.totalKms)
		return true;

	return false;
}

bool ViaLigacao::operator>(const ViaLigacao& vl) const
{
	if(totalKms > vl.totalKms)
		return true;

	return false;
}

void ViaLigacao::escrever(ostream& out) const
{
	out << "ORIGEM: " << origem->getDesc() << endl;
	out << "DESTINO: " << destino->getDesc() << endl;
	out << "CODIGO VIA: " << codVia << endl;
	out << "TOTAL KMS: " << totalKms << endl;
	out << "TEMPO MEDIO PERCURSO: ";
	if(tmpMedPerc<10)
		out << "00:0" << tmpMedPerc << "h" << endl;
	else
	{
		if(tmpMedPerc>=10 && tmpMedPerc<60)
			out << "00:" << tmpMedPerc << "h" << endl;
		else
		{
			if((tmpMedPerc/60)<10 && (tmpMedPerc%60)<10)
				out << "0" << tmpMedPerc/60 << ":0" << tmpMedPerc%60 << "h" << endl;
			if((tmpMedPerc/60)<10 && (tmpMedPerc%60)>=10)
				out << "0" << tmpMedPerc/60 << ":" << tmpMedPerc%60 << "h" << endl;
			if((tmpMedPerc/60)>=10 && (tmpMedPerc%60)>=10)
				out << tmpMedPerc/60 << ":" << tmpMedPerc%60 << "h" << endl;
			if((tmpMedPerc/60)>=10 && (tmpMedPerc%60)<10)
				out << tmpMedPerc/60 << ":0" << tmpMedPerc%60 << "h" << endl;
		}
	}
}

ostream& operator<<(ostream& o, const ViaLigacao& vl)
{
	vl.escrever(o);
	return o;
}

#endif
