#ifndef HistoricoCultural_
#define HistoricoCultural_

#include <iostream>
#include <string>

using namespace std;

#include "Local.h"

class HistoricoCultural : public Local
{
private:
	int tmpMedVisita;
	int abertura;
	int encerramento;

public:
	HistoricoCultural();
	HistoricoCultural(string descricao, int tmv, int ab, int enc);
	HistoricoCultural(const HistoricoCultural& hc);
	Local* clone() const;
	~HistoricoCultural();

	int getTmpMedVisita() const;
	int getAbertura() const;
	int getEncerramento() const;
	void setTmpMedVisita(int tmv);
	void setAbertura(int ab);
	void setEncerramento(int enc);

	void listar() const;

	HistoricoCultural& operator=(const HistoricoCultural& hc);

	void escrever(ostream& out) const;
};

HistoricoCultural::HistoricoCultural():Local()
{
	tmpMedVisita = 0;
	abertura = 0;
	encerramento = 0;
}

HistoricoCultural::HistoricoCultural(string descricao, int tmv, int ab, int enc):Local(descricao)
{
	tmpMedVisita = tmv;
	abertura = ab;
	encerramento = enc;
}

HistoricoCultural::HistoricoCultural(const HistoricoCultural& hc):Local(hc)
{
	tmpMedVisita = hc.tmpMedVisita;
	abertura = hc.abertura;
	encerramento = hc.encerramento;
}

Local* HistoricoCultural::clone() const
{
	return new HistoricoCultural(*this);
}

HistoricoCultural::~HistoricoCultural()
{

}

int HistoricoCultural::getTmpMedVisita() const 
{
	return tmpMedVisita;
}

int HistoricoCultural::getAbertura() const 
{
	return abertura;
}

int HistoricoCultural::getEncerramento() const 
{
	return encerramento;
}

void HistoricoCultural::setTmpMedVisita(int tmv) 
{
	tmpMedVisita = tmv;
}

void HistoricoCultural::setAbertura(int ab) 
{
	abertura = ab;
}

void HistoricoCultural::setEncerramento(int enc) 
{
	encerramento = enc;
}

void HistoricoCultural::listar() const
{
	cout << "LOCAL DE INTERESSE TURISTICO HISTORICO CULTURAL" << endl;
	Local::listar();
	cout << "TEMPO MEDIO VISITA: ";
	if(tmpMedVisita<10)
		cout << "00:0" << tmpMedVisita << "h" << endl;
	else
	{
		if(tmpMedVisita>=10 && tmpMedVisita<60)
			cout << "00:" << tmpMedVisita << "h" << endl;
		else
		{
			if((tmpMedVisita/60)<10 && (tmpMedVisita%60)<10)
				cout << "0" << tmpMedVisita/60 << ":0" << tmpMedVisita%60 << "h" << endl;
			if((tmpMedVisita/60)<10 && (tmpMedVisita%60)>=10)
				cout << "0" << tmpMedVisita/60 << ":" << tmpMedVisita%60 << "h" << endl;
			if((tmpMedVisita/60)>=10 && (tmpMedVisita%60)>=10)
				cout << tmpMedVisita/60 << ":" << tmpMedVisita%60 << "h" << endl;
			if((tmpMedVisita/60)>=10 && (tmpMedVisita%60)<10)
				cout << tmpMedVisita/60 << ":0" << tmpMedVisita%60 << "h" << endl;
		}
	}
	cout << "HORARIO ABERTURA: " << abertura/60 << ":";
	if((abertura%60)<10)
		cout << "0";
	cout << abertura%60 << "h" << endl;
	cout << "HORARIO ENCERRAMENTO: " << encerramento/60 << ":";
	if((encerramento%60)<10)
		cout << "0";
	cout << encerramento%60 << "h" << endl;
	cout << endl;
}

HistoricoCultural& HistoricoCultural::operator=(const HistoricoCultural& hc)
{
	if(this!=&hc)
	{
		tmpMedVisita = hc.tmpMedVisita;
		abertura = hc.abertura;
		encerramento = hc.encerramento;
		Local::operator=(hc);
	}
	return (*this);
}

void HistoricoCultural::escrever(ostream& out) const
{
	out << "LOCAL DE INTERESSE TURISTICO HISTORICO CULTURAL" << endl;
	Local::escrever(cout);
	out << "TEMPO MEDIO VISITA: ";
	if(tmpMedVisita<10)
		out << "00:0" << tmpMedVisita << "h" << endl;
	else
	{
		if(tmpMedVisita>=10 && tmpMedVisita<60)
			out << "00:" << tmpMedVisita << "h" << endl;
		else
		{
			if((tmpMedVisita/60)<10 && (tmpMedVisita%60)<10)
				out << "0" << tmpMedVisita/60 << ":0" << tmpMedVisita%60 << "h" << endl;
			if((tmpMedVisita/60)<10 && (tmpMedVisita%60)>=10)
				out << "0" << tmpMedVisita/60 << ":" << tmpMedVisita%60 << "h" << endl;
			if((tmpMedVisita/60)>=10 && (tmpMedVisita%60)>=10)
				out << tmpMedVisita/60 << ":" << tmpMedVisita%60 << "h" << endl;
			if((tmpMedVisita/60)>=10 && (tmpMedVisita%60)<10)
				out << tmpMedVisita/60 << ":0" << tmpMedVisita%60 << "h" << endl;
		}
	}
	out << "HORARIO ABERTURA: " << abertura/60 << ":";
	if((abertura%60)<10)
		out << "0";
	out << abertura%60 << "h" << endl;
	out << "HORARIO ENCERRAMENTO: " << encerramento/60 << ":";
	if((encerramento%60)<10)
		out << "0";
	out << encerramento%60 << "h" << endl;
}

#endif
