#ifndef Local_
#define Local_

#include <iostream>
#include <string>

using namespace std;

class Local
{
private:
	string desc;

public:
	Local();
	Local(string descricao);
	Local(const Local& l);
	virtual Local* clone() const;
	virtual ~Local();

	string getDesc() const;
	void setDesc(string descricao);

	virtual void listar() const;

	Local& operator=(const Local& l);

	bool operator==(const Local& l) const;
	bool operator<(const Local& l) const;
	bool operator>(const Local& l) const;

	virtual void escrever(ostream& out) const;
};

Local::Local()
{
	desc = "";
}

Local::Local(string descricao)
{
	desc = descricao;
}

Local::Local(const Local& l)
{
	desc=l.desc;
}

Local* Local::clone() const
{
	return new Local(*this);
}

Local::~Local()
{

}

string Local::getDesc() const 
{
	return desc;
}

void Local::setDesc(string descricao) 
{
	desc = descricao;
}

void Local::listar() const
{
	cout << "DESCRICAO: " << desc << endl;
}

Local& Local::operator=(const Local& l)
{
	if(this!=&l)
	{
		desc=l.desc;
	}
	return (*this);
}

bool Local::operator==(const Local& l) const
{
	return desc==l.desc;
}

bool Local::operator<(const Local& l) const
{
	if(desc<l.desc)
		return true;

	return false;
}

bool Local::operator>(const Local& l) const
{
	if(desc>l.desc)
		return true;

	return false;
}

void Local::escrever(ostream& out) const
{
	out << "DESCRICAO: " << desc << endl;
}

ostream& operator<<(ostream& o, const Local& l)
{
	l.escrever(o);
	return o;
}

#endif
