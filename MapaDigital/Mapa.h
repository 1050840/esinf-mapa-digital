#ifndef _Mapa_
#define _Mapa_

#include <string>

using namespace std;

#include "graphStlPath.h"
#include "PViaLigacao.h"
#include "HistoricoCultural.h"

class Mapa : public graphStlPath <Local*, PViaLigacao>
{
private:
	typedef list < graphVertex <Local*, PViaLigacao> >::iterator vIterator;
	typedef list < graphEdge <Local*, PViaLigacao> >::iterator eIterator;

	int getChave(const string& s);
	bool existeCaminho(vIterator it1, vIterator it2, bitset <MAX_VERTICES>& taken);
	bool existeCaminho(vIterator it1, vIterator it2);
	string intToTime(int tmp);
	void mostraCaminho(int vi, const vector <int>& path);
	int menorCaminho(const string& origem, const string& destino, bool print);
	void invertStack(stack<Local*>& s);

protected:
	bool compareVertices(Local* const& vContent1, Local* const& vContent2);

public:
	Mapa();
	Mapa(const Mapa& m);

	void write(ostream& out);

	bool existeRamo(ViaLigacao* const& vl, Local* const& lo, Local* const& ld);
	bool existeVertice(const string& s);
	bool existeCaminho(const string& origem, const string& destino);
	void menorCaminhoDistancia(const string& origem, const string& destino);
	void menorCaminhoTempo(const string& origem, const string& destino);
	void menorCaminhoCusto(const string& origem, const string& destino);
	bool caminhoMaiorInteresseTuristico(const string& origem, const string& destino);
	bool percursosPossiveis(const string& origem, const string& destino);
	void itinerario(int instanteInicial, Local* const& locInicial, stack<Local*>& s, int opcao);
};

Mapa::Mapa() : graphStlPath <Local*, PViaLigacao>() {

}

Mapa::Mapa(const Mapa& m) : graphStlPath <Local*, PViaLigacao>(m) {

}

bool Mapa::compareVertices(Local* const& vContent1, Local* const& vContent2) {
	return (*vContent1 == *vContent2);
}

void Mapa::write(ostream& out) {
	out << "Infinito: " << getInfinite() << endl;
	for (vIterator itv = vlist.begin(); itv != vlist.end(); itv++)
	{
		out << endl;
		out << "Vertice: " << itv->getVContent()->getDesc() << " Chave: " << itv->getVKey() << endl;
		out << "Ligacoes: " << endl;
		for (eIterator ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++)
			out << "Ramo: " << (*ite).getEContent() << " liga a " << ite->getVDestination()->getVContent()->getDesc() << endl;
	}
}

int Mapa::getChave(const string& s) {
	for (vIterator itv = vlist.begin(); itv != vlist.end(); itv++)
	if (itv->getVContent()->getDesc() == s)
		return itv->getVKey();
	return -1;
}

bool Mapa::existeCaminho(vIterator it1, vIterator it2, bitset <MAX_VERTICES>& taken) {
	if (it1 == it2)
		return true;

	taken[it1->getVKey()] = true;

	for (auto ite = it1->getAdjacenciesBegin(); ite != it1->getAdjacenciesEnd(); ite++) {
		vIterator itv = ite->getVDestination();
		if (!taken[itv->getVKey()]) {
			bool saida = existeCaminho(itv, it2, taken);
			if (saida)
				return true;
		}
	}

	return false;
}

bool Mapa::existeCaminho(vIterator it1, vIterator it2) {
	bitset <MAX_VERTICES> taken;

	return existeCaminho(it1, it2, taken);
}

bool Mapa::existeCaminho(const string& origem, const string& destino) {
	Local* lo;
	Local* ld;

	this->getVertexContentByKey(lo, getChave(origem));
	this->getVertexContentByKey(ld, getChave(destino));

	vIterator it1, it2;
	this->getVertexIteratorByContent(it1, lo);
	this->getVertexIteratorByContent(it2, ld);

	return this->existeCaminho(it1, it2);
}

bool Mapa::existeRamo(ViaLigacao* const& vl, Local* const& lo, Local* const& ld) {
	for (vIterator itv = vlist.begin(); itv != vlist.end(); itv++)
	for (eIterator ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++)
	if ((itv->getVContent()->getDesc() == lo->getDesc()) && (ite->getEContent().getCodVia() == vl->getCodVia()) && (ite->getVDestination()->getVContent()->getDesc() == ld->getDesc()))
		return true;

	return false;
}

bool Mapa::existeVertice(const string& s) {
	for (vIterator itv = vlist.begin(); itv != vlist.end(); itv++)
	if (itv->getVContent()->getDesc() == s)
		return true;
	return false;
}

string Mapa::intToTime(int tmp) {
	string s;
	if (tmp < 10)
		s += "00:0" + to_string(tmp) + "h";
	else
	{
		if (tmp >= 10 && tmp < 60)
			s += "00:" + to_string(tmp) + "h";
		else
		{
			if ((tmp / 60) < 10 && (tmp % 60) < 10)
				s += "0" + to_string(tmp / 60) + ":0" + to_string(tmp % 60) + "h";
			if ((tmp / 60) < 10 && (tmp % 60) >= 10)
				s += "0" + to_string(tmp / 60) + ":" + to_string(tmp % 60) + "h";
			if ((tmp / 60) >= 10 && (tmp % 60) >= 10)
				s += to_string(tmp / 60) + ":" + to_string(tmp % 60) + "h";
			if ((tmp / 60) >= 10 && (tmp % 60) < 10)
				s += to_string(tmp / 60) + ":0" + to_string(tmp % 60) + "h";
		}
	}
	return s;
}

void Mapa::mostraCaminho(int vi, const vector <int>& path) {
	if (path[vi] == -1)
		return;

	mostraCaminho(path[vi], path);

	PViaLigacao vl;

	bool flag = true;

	list < graphVertex <Local*, PViaLigacao> >::iterator itvo, itvd;

	if (!(getVertexIteratorByKey(itvo, path[vi]) && getVertexIteratorByKey(itvd, vi)))
		flag = false;

	if (flag == true)
	{
		stack<PViaLigacao> s;
		for (list < graphEdge <Local*, PViaLigacao> >::iterator ite = itvo->getAdjacenciesBegin(); ite != itvo->getAdjacenciesEnd(); ite++)
			if (ite->getVDestination() == itvd) {
				s.push(ite->getEContent());
		}
			if (s.size() > 1) {
				PViaLigacao pv;
				while (!s.empty()) {
					pv = s.top();
					s.pop();
					if (!s.empty()) {
						if (pv > s.top()) {
							vl = s.top();
							s.pop();
						}
						else{
							vl = pv;
							s.pop();
						}

					}
				}
			}
			else {
				vl = s.top();
				s.pop();
			}

	}

	Local* l;

	this->getVertexContentByKey(l, vi);

	cout << "   -> " << vl << " ->" << endl << " " << l->getDesc() << endl;
}

int Mapa::menorCaminho(const string& origem, const string& destino, bool print) {
	Local* lo;
	Local* ld;

	vector <int> path;
	vector <PViaLigacao> dist;
	int key;

	int tmp;

	this->getVertexContentByKey(lo, getChave(origem));
	this->getVertexContentByKey(ld, getChave(destino));

	this->getVertexKeyByContent(key, ld);
	this->dijkstrasAlgorithm(lo, path, dist);
	tmp = dist[key].getTempo();

	if (print)
	{
		cout << endl << "   " << lo->getDesc() << " e " << ld->getDesc() << " : " << dist[key].getKM() << " KMS ; " << intToTime(tmp) << " ; " << fixed << setprecision(2) << dist[key].getCusto() << " EUR" << endl << endl;
		cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
		cout << " " << lo->getDesc() << endl;
		mostraCaminho(key, path);
	}

	return tmp;
}

void Mapa::menorCaminhoDistancia(const string& origem, const string& destino) {
	PViaLigacao::setComparacaoKMS();
	cout << " Menor distancia entre ";
	menorCaminho(origem, destino, true);
}

void Mapa::menorCaminhoTempo(const string& origem, const string& destino) {
	PViaLigacao::setComparacaoTEMPO();
	cout << " Menor tempo entre ";
	menorCaminho(origem, destino, true);
}

void Mapa::menorCaminhoCusto(const string& origem, const string& destino) {
	PViaLigacao::setComparacaoCUSTO();
	cout << " Menor custo entre ";
	menorCaminho(origem, destino, true);
}

void Mapa::invertStack(stack<Local*>& s) {
	stack<Local*> sTmp;

	while (!s.empty()) {
		sTmp.push(s.top());
		s.pop();
	}
	s = sTmp;
	return;
}

bool Mapa::caminhoMaiorInteresseTuristico(const string& origem, const string& destino) {
	bool flag = false;

	Local* lo;
	Local* ld;

	this->getVertexContentByKey(lo, getChave(origem));
	this->getVertexContentByKey(ld, getChave(destino));

	queue <stack <Local*>> caminhos = distinctPaths(lo, ld);
	queue <stack <Local*>> qaux;

	unsigned int maior = 0;

	if (!caminhos.empty()) {
		flag = true;
		stack<Local*> cam;
		int i = 0;

		while (!caminhos.empty()) {
			cam = caminhos.front();

			if (cam.size() >= maior) {
				maior = cam.size();
				qaux.push(cam);
			}
			caminhos.pop();
		}

		cout << " Maior interesse turistico entre " << lo->getDesc() << " e " << ld->getDesc() << " (" << maior << " locais):" << endl;
		cout << endl;

		while (!qaux.empty()) {
			cam = qaux.front();
			
			if (cam.size() == maior) {
				invertStack(cam); // percurso origem -> destino
				cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;

				while (!cam.empty()) {
					Local* l = cam.top();
					cout << " " << l->getDesc() << endl;
					cam.pop();
					
					if (!cam.empty()) {
						PViaLigacao vl;
						this->getEdgeByVertexContents(vl, l, cam.top());
						cout << "   -> " << vl << " ->" << endl;
					}
				}
				cout << endl;
			}
			qaux.pop();
		}
	}
	return flag;
}

bool Mapa::percursosPossiveis(const string& origem, const string& destino) {
	bool flag = false;

	Local* lo;
	Local* ld;

	this->getVertexContentByKey(lo, getChave(origem));
	this->getVertexContentByKey(ld, getChave(destino));

	queue <stack <Local*>> caminhos = distinctPaths(lo, ld);

	if (!caminhos.empty()) {
		flag = true;
		cout << " Existem " << caminhos.size() << " percursos possiveis entre " << lo->getDesc() << " e " << ld->getDesc() << ":" << endl;
		cout << endl;

		stack<Local*> cam;

		while (!caminhos.empty()) {
			cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
			cam = caminhos.front();
			caminhos.pop();

			invertStack(cam); // percurso origem -> destino

			Local* l = cam.top();
			cout << " " << l->getDesc() << endl;

			while (!cam.empty()) {
				cam.pop();

				if (!cam.empty()) {
					PViaLigacao vl;
					this->getEdgeByVertexContents(vl, l, cam.top());
					cout << "   -> " << vl << " ->" << endl;
					l = cam.top();
					cout << " " << l->getDesc();
				}
				cout << endl;
			}
		}
	}
	return flag;
}

void Mapa::itinerario(int instanteInicial, Local* const& locInicial, stack<Local*>& s, int opcao) {
	switch (opcao)
	{
		case 1:
		{
			PViaLigacao::setComparacaoKMS();
			break;
		}

		case 2:
		{
			PViaLigacao::setComparacaoTEMPO();
			break;
		}
		
		case 3:
		{
			PViaLigacao::setComparacaoCUSTO();
			break;
		}
		
		default:
		{
			break;
		}
	}

	invertStack(s); // percurso origem -> destino

	int instanteAtual = instanteInicial;

	bool visitaLocal;

	cout << "                              ITINERARIO DE VIAGEM                             " << endl;
	cout << "_______________________________________________________________________________" << endl;
	cout << endl;
	cout << " *   " << intToTime(instanteAtual) << " - INICIO" << endl;
	cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;

	if (locInicial != s.top()) {
		cout << " Viagem entre ";
		instanteAtual = instanteAtual + menorCaminho(locInicial->getDesc(), s.top()->getDesc(), true);
		cout << endl;
		cout << "_______________________________________________________________________________" << endl;
		cout << endl;
		cout << " *   " << (((instanteAtual / 24 / 60) > 0) ? intToTime(instanteAtual % (24 * 60)) : intToTime(instanteAtual)) << endl;
		cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
		cout << " Chegada ao";
	}
	
	while (!s.empty()) {
		Local* l = s.top();

		if (typeid(*l) == typeid(HistoricoCultural)) {
			int abertura = (dynamic_cast <HistoricoCultural*>(l)->getAbertura());
			int tmpMedVisita = (dynamic_cast <HistoricoCultural*>(l)->getTmpMedVisita());
			int encerramento = (dynamic_cast <HistoricoCultural*>(l)->getEncerramento());
			int tempoEspera = 0;

			if (instanteAtual <= abertura) {
				visitaLocal = true;
				tempoEspera = abertura - instanteAtual;
			}
			else {
				if ((instanteAtual + tmpMedVisita) <= encerramento)
					visitaLocal = true;
				else
					visitaLocal = false;
			}

			cout << " LOCAL DE INTERESSE HISTORICO E CULTURAL:" << endl << endl << " " << l->getDesc() << endl << " (abertura: " << intToTime(abertura) << " encerramento: " << intToTime(encerramento) << " tempo medio de visita: " << intToTime(tmpMedVisita) << ")";

			if (visitaLocal == true) {
				instanteAtual = instanteAtual + tempoEspera;
				cout << endl << endl << " -> Visita ao local entre as " << intToTime(instanteAtual) << " e as " << intToTime(instanteAtual + tmpMedVisita) << endl;
				instanteAtual = instanteAtual + tmpMedVisita;
				cout << endl << "_______________________________________________________________________________" << endl;
			}
			else {
				cout << endl << endl << " -> Visita nao permitida" << endl;
				cout << endl << "_______________________________________________________________________________" << endl;
			}

		}
		else {
			cout << " LOCAL DE INTERESSE NATURAL:" << endl << endl << " " << l->getDesc() << endl << endl << " -> Visita ao local" << endl;
			cout << endl << "_______________________________________________________________________________" << endl;
		}

		s.pop();

		if (!s.empty()) {
			cout << endl;
			cout << " *   " << (((instanteAtual / 24 / 60) > 0) ? intToTime(instanteAtual % (24 * 60)) : intToTime(instanteAtual)) << endl;
			cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
			cout << " Viagem entre ";

			instanteAtual = instanteAtual + menorCaminho(l->getDesc(), s.top()->getDesc(), true);

			cout << endl;
			cout << "_______________________________________________________________________________" << endl;
			cout << endl;
			cout << " *   " << (((instanteAtual / 24 / 60) > 0) ? intToTime(instanteAtual % (24 * 60)) : intToTime(instanteAtual)) << endl;
			cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
			cout << " Chegada ao";
		}

	}

	cout << endl << " => Itinerario tem inicio as " << intToTime(instanteInicial) << " e termina";

	if ((instanteAtual / 24 / 60) > 1)
		cout << " " << (instanteAtual / 24 / 60) << " dias depois";

	cout << " as " << intToTime(instanteAtual % (24 * 60));

	if ((instanteAtual / 24 / 60) == 1)
		cout << " do dia seguinte";

	cout << endl;
}

#endif
