#ifndef _PViaLigacao_
#define _PViaLigacao_

#include <ostream>

using namespace std;

#include "ViaLigacao.h"
#include "AE.h"

// Mandatory:
// Constructor without parameters, copy constructor & destructor
// Operators: >, <, +, += e =
// The use of cloning is advised

class PViaLigacao {
private:
	ViaLigacao *apvl;

	enum TipoComparacao { KMS , TEMPO , CUSTO }; 
	static TipoComparacao  tipoComparacao;

public:
	static void setComparacaoKMS();
	static void setComparacaoTEMPO();
	static void setComparacaoCUSTO();

	int getKM() const;
	int getTempo() const;
	float getCusto() const;
	string getCodVia() const;

	PViaLigacao();
	PViaLigacao(const ViaLigacao& vl);
	PViaLigacao(int km, int tempo, float custo);
	PViaLigacao(const PViaLigacao& vl);
	~PViaLigacao();

	bool operator>(const PViaLigacao& vl) const;
	bool operator<(const PViaLigacao& vl) const;
	bool operator==(const PViaLigacao& vl) const;
	PViaLigacao operator+(const PViaLigacao& vl);
	const PViaLigacao& operator+=(const PViaLigacao& vl);
	const PViaLigacao& operator=(const PViaLigacao& vl);
	void write(ostream& out) const;
};

PViaLigacao::TipoComparacao PViaLigacao::tipoComparacao=PViaLigacao::TipoComparacao::KMS;

void PViaLigacao::setComparacaoKMS() {
	tipoComparacao=TipoComparacao::KMS;
}

void PViaLigacao::setComparacaoTEMPO() {
	tipoComparacao = TipoComparacao::TEMPO;
}

void PViaLigacao::setComparacaoCUSTO() {
	tipoComparacao=TipoComparacao::CUSTO;
}

int PViaLigacao::getKM() const {
	return apvl->getTotalKms();
}

int PViaLigacao::getTempo() const {
	return apvl->getTmpMedPerc();
}

float PViaLigacao::getCusto() const {
	if (typeid(*apvl) == typeid(AE))
		return (dynamic_cast <AE*>(apvl)->getPortagem());
	else
		return 0;
}

string PViaLigacao::getCodVia() const {
	return apvl->getCodVia();
}

PViaLigacao::PViaLigacao() {
	this->apvl = new AE();
}

PViaLigacao::PViaLigacao(int km, int tempo, float custo) {
	apvl = new AE(NULL, NULL, "", km, tempo, custo);
}

PViaLigacao::PViaLigacao(const ViaLigacao& vl) {
	this->apvl = vl.clone();
}

PViaLigacao::PViaLigacao(const PViaLigacao& vl) {
	this->apvl = vl.apvl->clone();
}

PViaLigacao::~PViaLigacao() {
	delete apvl;
}

bool PViaLigacao::operator>(const PViaLigacao& vl) const {	
	if (tipoComparacao == TipoComparacao::KMS)
		return this->apvl->getTotalKms() > vl.apvl->getTotalKms();

	if (tipoComparacao == TipoComparacao::TEMPO)
		return this->apvl->getTmpMedPerc() > vl.apvl->getTmpMedPerc();

	float tgp = 0;
	float vlgp = 0;

	if (typeid(*apvl) == typeid(AE))
		tgp = (dynamic_cast <AE*>(apvl)->getPortagem());

	if (typeid(*(vl.apvl)) == typeid(AE))
		vlgp = (dynamic_cast <AE*>(vl.apvl)->getPortagem());

	return tgp > vlgp;
}

bool PViaLigacao::operator<(const PViaLigacao& vl) const {
	if (tipoComparacao == TipoComparacao::KMS)
		return this->apvl->getTotalKms() < vl.apvl->getTotalKms();

	if (tipoComparacao == TipoComparacao::TEMPO)
		return this->apvl->getTmpMedPerc() < vl.apvl->getTmpMedPerc();

	float tgp = 0;
	float vlgp = 0;

	if (typeid(*apvl) == typeid(AE))
		tgp = (dynamic_cast <AE*>(apvl)->getPortagem());

	if (typeid(*(vl.apvl)) == typeid(AE))
		vlgp = (dynamic_cast <AE*>(vl.apvl)->getPortagem());

	return tgp < vlgp;
}

bool PViaLigacao::operator==(const PViaLigacao& vl) const {
	if (tipoComparacao == TipoComparacao::KMS)
		return this->apvl->getTotalKms() == vl.apvl->getTotalKms();

	if (tipoComparacao == TipoComparacao::TEMPO)
		return this->apvl->getTmpMedPerc() == vl.apvl->getTmpMedPerc();

	float tgp = 0;
	float vlgp = 0;

	if (typeid(*apvl) == typeid(AE))
		tgp = (dynamic_cast <AE*>(apvl)->getPortagem());

	if (typeid(*(vl.apvl)) == typeid(AE))
		vlgp = (dynamic_cast <AE*>(vl.apvl)->getPortagem());

	return tgp == vlgp;
}

PViaLigacao PViaLigacao::operator+(const PViaLigacao& vl) {
	float tgp = 0;
	float vlgp = 0;

	if (typeid(*apvl) == typeid(AE))
		tgp = (dynamic_cast <AE*>(apvl)->getPortagem());

	if (typeid(*(vl.apvl)) == typeid(AE))
		vlgp = (dynamic_cast <AE*>(vl.apvl)->getPortagem());

	return PViaLigacao(this->apvl->getTotalKms() + vl.apvl->getTotalKms(), this->apvl->getTmpMedPerc() + vl.apvl->getTmpMedPerc(), tgp + vlgp);
}

const PViaLigacao& PViaLigacao::operator+=(const PViaLigacao& vl) {
	this->apvl->setTotalKms(this->apvl->getTotalKms() + vl.apvl->getTotalKms());
	this->apvl->setTmpMedPerc(this->apvl->getTmpMedPerc() + vl.apvl->getTmpMedPerc());
	if (typeid(*apvl)==typeid(AE)) {
		AE *ae = (AE *)this->apvl;
		float vlgp = 0;
		if (typeid(*(vl.apvl)) == typeid(AE))
			vlgp = (dynamic_cast <AE*>(vl.apvl)->getPortagem());

		ae->setPortagem(ae->getPortagem() + vlgp);
	}	
	return  *this;
}

const PViaLigacao& PViaLigacao::operator=(const PViaLigacao& vl) {
	this->apvl = vl.apvl->clone();
	return *this;
}

void PViaLigacao::write(ostream& out) const {
	int tmp;
	string s;
	tmp = (*apvl).getTmpMedPerc();
	if (tmp < 10)
		s += "00:0" + to_string(tmp) + "h";
	else
	{
		if (tmp >= 10 && tmp < 60)
			s += "00:" + to_string(tmp) + "h";
		else
		{
			if ((tmp / 60)<10 && (tmp % 60)<10)
				s += "0" + to_string(tmp / 60) + ":0" + to_string(tmp % 60) + "h";
			if ((tmp / 60)<10 && (tmp % 60) >= 10)
				s += "0" + to_string(tmp / 60) + ":" + to_string(tmp % 60) + "h";
			if ((tmp / 60) >= 10 && (tmp % 60) >= 10)
				s += to_string(tmp / 60) + ":" + to_string(tmp % 60) + "h";
			if ((tmp / 60) >= 10 && (tmp % 60)<10)
				s += to_string(tmp / 60) + ":0" + to_string(tmp % 60) + "h";
		}
	}

	if (typeid(*apvl) == typeid(AE))
		out << (*apvl).getCodVia() << " (" << (*apvl).getTotalKms() << " KMS ; " << s << " ; " << fixed << setprecision(2) << dynamic_cast <AE*>(apvl)->getPortagem() << " EUR)";
	else
		out << (*apvl).getCodVia() << " (" << (*apvl).getTotalKms() << " KMS ; " << s << ")";
}

ostream& operator<<(ostream& out, const PViaLigacao& vl) {
	vl.write(out);
	return out;
}

#endif
