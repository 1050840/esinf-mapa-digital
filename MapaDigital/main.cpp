#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <regex>

using namespace std;

#include "Local.h"
#include "Natural.h"
#include "HistoricoCultural.h"
#include "ViaLigacao.h"
#include "AE.h"
#include "EN.h"
#include "PViaLigacao.h"
#include "Teste.h"
#include "Mapa.h"

void runMenu();
void runMenuFxs();
void runMenuInsLoc();
void runMenuInsVia();
void runMenuFxsOm();
void runMenuFxLoc();
void runMenuFxVia();
void cabecalho();
void menu();
void menuFxs();
void menuInsLoc();
void menuInsLocHistCult();
void menuInsLocNat();
void menuInsVia();
void menuInsViaAE();
void menuInsViaEN();
void menuInsOrigemDestino(int& opcao);
void menuItinerario();
void runMenuTemas();
void menuTemas();
void rodape();
int lerOpcao();

void trim(string& str);

Teste t;
Mapa m;

void main()
{
	runMenu();
}

void cabecalho()
{
	cout << "_______________________________________________________________________________" << endl;
	cout << "                                                                               " << endl;
	cout << "           ooo        ooooo       .o.       ooooooooo.     .o.                 " << endl;
	cout << "           `88.       .888'      .888.      `888   `Y88.  .888.                " << endl;
	cout << "            888b     d'888      .8'888.      888   .d88' .8'888.               " << endl;
	cout << "            8 Y88. .P  888     .8' `888.     888ooo88P' .8' `888.              " << endl;
	cout << "            8  `888'   888    .88ooo8888.    888       .88ooo8888.             " << endl;
	cout << "            8    Y     888   .8'     `888.   888      .8'     `888.            " << endl;
	cout << "           o8o        o888o o88o     o8888o o888o    o88o     o8888o           " << endl;
	cout << "                                                                               " << endl;
	cout << "                                                                               " << endl;
	cout << "  oooooooooo.   ooooo   .oooooo.    ooooo ooooooooooooo   .o.       ooooo      " << endl;
	cout << "  `888'   `Y8b  `888'  d8P'  `Y8b   `888' 8'   888   `8  .888.      `888'      " << endl;
	cout << "   888      888  888  888            888       888      .8'888.      888       " << endl;
	cout << "   888      888  888  888            888       888     .8' `888.     888       " << endl;
	cout << "   888      888  888  888     ooooo  888       888    .88ooo8888.    888       " << endl;
	cout << "   888     d88'  888  `88.    .88'   888       888   .8'     `888.   888     o " << endl;
	cout << "  o888bood8P'   o888o  `Y8bood8P'   o888o     o888o o88o     o8888o o888ooood8 " << endl;
	cout << "_______________________________________________________________________________" << endl;
	cout << "                                                                               " << endl;
}

void menu()
{
	cout << "                                      MENU                                     " << endl;
	cout << "                                                                               " << endl;
	cout << "   1 - Carregar Ficheiros por omissao                                          " << endl;
	cout << "                                                                               " << endl;
	cout << "   2 - Carregar Ficheiros (indicar localizacao)                                " << endl;
	cout << "                                                                               " << endl;
	cout << "   3 - Inserir Local                                                           " << endl;
	cout << "                                                                               " << endl;
	cout << "   4 - Inserir Via                                                             " << endl;
	cout << "                                                                               " << endl;
	cout << "   5 - Listar Locais                                                           " << endl;
	cout << "                                                                               " << endl;
	cout << "   6 - Listar Vias                                                             " << endl;
	cout << "                                                                               " << endl;
	cout << "   7 - Contabilizar Locais de Interesse Historico e Culturais                  " << endl;
	cout << "                                                                               " << endl;
	cout << "   8 - Ordenar Locais por ordem alfabetica da descricao                        " << endl;
	cout << "                                                                               " << endl;
	cout << "   9 - Visualizar Grafo                                                        " << endl;
	cout << "                                                                               " << endl;
	cout << "  10 - Listar todos os Percursos possiveis entre dois Locais                   " << endl;
	cout << "                                                                               " << endl;
	cout << "  11 - Calcular Percurso mais curto em KMS entre dois Locais                   " << endl;
	cout << "                                                                               " << endl;
	cout << "  12 - Calcular Percurso mais rapido em TEMPO entre dois Locais                " << endl;
	cout << "                                                                               " << endl;
	cout << "  13 - Calcular Percurso mais economico em EUROS entre dois Locais             " << endl;
	cout << "                                                                               " << endl;
	cout << "  14 - Calcular Percurso de maior interesse turistico entre dois Locais        " << endl;
	cout << "                                                                               " << endl;
	cout << "  15 - Criar Itinerario e calcular instante final da Viagem                    " << endl;
	cout << "                                                                               " << endl;
	cout << "  16 - Opcoes da Aplicacao                                                     " << endl;
	cout << "                                                                               " << endl;
	cout << "   0 - Sair                                                                    " << endl;
}

void rodape()
{
	cout << "_______________________________________________________________________________" << endl;
	cout << "                                                                               " << endl;
	cout << "  ESINF - ESTRUTURAS DE INFORMACAO                 TRABALHO PRATICO 2013/2014  " << endl;
	cout << "                                                                               " << endl;
	cout << "  1050840 - Jose Vieira                                 1090533 - Andre Sousa  " << endl;
	cout << "_______________________________________________________________________________" << endl;
	cout << "                                                                               " << endl;
	cout << "                   INSTITUTO SUPERIOR DE ENGENHARIA DO PORTO                   " << endl;
	cout << "_______________________________________________________________________________" << endl;
}

void menuFxs()
{
	cout << "                             MENU CARREGAR FICHEIROS                           " << endl;
	cout << "                                                                               " << endl;
	cout << "  1 - Carregar Ficheiro de Locais                                              " << endl;
	cout << "                                                                               " << endl;
	cout << "  2 - Carregar Ficheiro de Vias de Ligacao                                     " << endl;
	cout << "                                                                               " << endl;
	cout << "  0 - Voltar                                                                   " << endl;
}

void menuInsLoc()
{
	cout << "                              MENU INSERIR LOCAL                               " << endl;
	cout << "                                                                               " << endl;
	cout << "  1 - Inserir Local de Interesse Turistico Natural                             " << endl;
	cout << "                                                                               " << endl;
	cout << "  2 - Inserir Local de Interesse Turistico Historico e Cultural                " << endl;
	cout << "                                                                               " << endl;
	cout << "  0 - Voltar                                                                   " << endl;
}

void menuInsVia()
{
	cout << "                          MENU INSERIR VIA DE LIGACAO                          " << endl;
	cout << "                                                                               " << endl;
	cout << "  1 - Inserir Auto-Estrada                                                     " << endl;
	cout << "                                                                               " << endl;
	cout << "  2 - Inserir Estrada Nacional                                                 " << endl;
	cout << "                                                                               " << endl;
	cout << "  0 - Voltar                                                                   " << endl;
}

int lerOpcao()
{
	int int_opcao;
	string str_opcao;
	
	getline(cin, str_opcao); //get string input
	trim(str_opcao);
	stringstream convert(str_opcao); //turns the string into a stream
	
	//checks for complete conversion to integer
	if(convert >> int_opcao && !(convert >> str_opcao))
		return int_opcao;
	
	cin.clear(); //just in case an error occurs with cin (eof(), etc)
	int_opcao=-1;
	return int_opcao;
}

void runMenu()
{
	int opcao;

	do
	{
		system("CLS");
		cabecalho();
		menu();
		rodape();
		opcao=lerOpcao();

			switch (opcao)
			{
				case 1:
				{
					system("CLS");
					cabecalho();
					runMenuFxsOm();
					rodape();
					system("PAUSE");
					break;
				}

				case 2:
				{
					runMenuFxs();
					break;
				}

				case 3:
				{
					runMenuInsLoc();
					break;
				}

				case 4:
				{
					if(t.getActLoc()>1)
						runMenuInsVia();
					else
					{
						system("CLS");
						cabecalho();
						cout << "                          MENU INSERIR VIA DE LIGACAO                          " << endl;
						cout << "                                                                               " << endl;
						cout << "  => ERRO! DEVEM EXISTIR PELO MENOS DOIS LOCAIS NA APLICACAO                   " << endl;
						rodape();
						system("PAUSE");
					}
					break;
				}

				case 5:
				{
					system("CLS");
					cabecalho();
					t.listarLocais();
					rodape();
					system("PAUSE");
					break;
				}
			
				case 6:
				{
					system("CLS");
					cabecalho();
					t.listarVias();
					rodape();
					system("PAUSE");
					break;
				}

				case 7:
				{
					system("CLS");
					cabecalho();
					t.contabilizarLocais();
					rodape();
					system("PAUSE");
					break;
				}
			
				case 8:
				{
					system("CLS");
					cabecalho();
					t.ordenarLocais();
					t.listarLocais();
					rodape();
					system("PAUSE");
					break;
				}

				case 9:
				{
					system("CLS");
					cabecalho();
					m.write(cout);
					rodape();
					system("PAUSE");
					break;
				}

				case 10:
				{
					menuInsOrigemDestino(opcao);
					system("PAUSE");
					break;
				}

				case 11:
				{
					menuInsOrigemDestino(opcao);
					system("PAUSE");
					break;
				}

				case 12:
				{
					menuInsOrigemDestino(opcao);
					system("PAUSE");
					break;
				}

				case 13:
				{
					menuInsOrigemDestino(opcao);
					system("PAUSE");
					break;
				}

				case 14:
				{
					menuInsOrigemDestino(opcao);
					system("PAUSE");
					break;
				}

				case 15:
				{
					menuItinerario();
					system("PAUSE");
					break;
				}

				case 16:
				{
					runMenuTemas();
					break;
				}

				case 0:
				{
				//	break;
				}
			
				default:
				{
					break;
				}
			}
		
	}
	while (opcao != 0);
}

void runMenuFxs()
{
	int opcao;

	do
	{
		system("CLS");
		cabecalho();
		menuFxs();
		rodape();
		opcao=lerOpcao();

			switch (opcao)
			{
				case 1:
				{
					runMenuFxLoc();
					system("PAUSE");
					break;
				}
			
				case 2:
				{
					runMenuFxVia();
					system("PAUSE");
					break;
				}

				case 0:
				{
					break;
				}
			
				default:
				{
					break;
				}
			}
		
	}
	while (opcao !=0);
}

void runMenuInsLoc()
{
	int opcao;

	do
	{
		system("CLS");
		cabecalho();
		menuInsLoc();
		rodape();
		opcao=lerOpcao();

			switch (opcao)
			{
				case 1:
				{
					menuInsLocNat();
					system("PAUSE");
					break;
				}
			
				case 2:
				{
					menuInsLocHistCult();
					system("PAUSE");
					break;
				}

				case 0:
				{
					break;
				}
			
				default:
				{
					break;
				}
			}
		
	}
	while (opcao!=0);
}

void runMenuInsVia()
{
	int opcao;

	do
	{
		system("CLS");
		cabecalho();
		menuInsVia();
		rodape();
		opcao=lerOpcao();

			switch (opcao)
			{
				case 1:
				{
					menuInsViaAE();
					system("PAUSE");
					break;
				}
			
				case 2:
				{
					menuInsViaEN();
					system("PAUSE");
					break;
				}

				case 0:
				{
					break;
				}
			
				default:
				{
					break;
				}
			}
		
	}
	while (opcao!=0);
}

void runMenuFxsOm()
{
	string loc="locais";
	string via="vias";
	int retLoc;
	int retVia;

	retLoc=t.lerFxLocais(loc);
	retVia=t.lerFxVias(via);

	cout << "                       MENU CARREGAR FICHEIROS POR OMISSAO                     " << endl;
	cout << "                                                                               " << endl;
	
	if(retLoc!=-1 && retVia!=-1)
	{
		cout << "  FICHEIROS " << loc << ".txt E " << via << ".txt ENCONTRADOS!" << endl;
		cout << endl;
		cout << "  FORAM CARREGADOS " << retLoc << " NOVOS LOCAIS NA APLICACAO" << endl;
		cout << "  FORAM CARREGADAS " << retVia << " NOVAS VIAS DE LIGACAO NA APLICACAO" << endl;

		if (retVia > 0)
		{
			for (int i = 0; i<t.getActVia(); i++)
				if (!m.existeRamo(t.getVecVias()[i], t.getVecVias()[i]->getOrigem(), t.getVecVias()[i]->getDestino()))
				{
					m.addGraphEdge(PViaLigacao(*((t.getVecVias())[i])), t.getVecVias()[i]->getOrigem(), t.getVecVias()[i]->getDestino());
					m.addGraphEdge(PViaLigacao(*((t.getVecVias())[i])), t.getVecVias()[i]->getDestino(), t.getVecVias()[i]->getOrigem());
				}
		}

		if (retLoc > 0)
		{
			for (int i = 0; i < t.getActLoc(); i++)
				m.addGraphVertex(t.getVecLocais()[i]);
		}
	}
	else if(retLoc==-1 && retVia==-1)
		{
			cout << "  !!! ERRO !!!" << endl;
			cout << endl;
			cout << "  -> FICHEIROS " << loc << ".txt E " << via << ".txt NAO ENCONTRADOS!" << endl;
		}
		else if(retLoc==-1 && retVia!=-1)
			{
				cout << "  !!! ERRO !!!" << endl;
				cout << endl;
				cout << "  -> FICHEIRO " << loc << ".txt NAO ENCONTRADO!" << endl;
				cout << endl;
				cout << "  -> FICHEIRO " << via << ".txt NAO PODE SER CARREGADO!" << endl;
			}
			else if(retLoc!=-1 && retVia==-1)
				{
					cout << "  !!! ERRO !!!" << endl;
					cout << endl;
					cout << "  -> FICHEIRO " << via << ".txt NAO ENCONTRADO!" << endl;
					cout << endl;
					cout << endl;
					cout << "  FICHEIRO " << loc << ".txt ENCONTRADO!" << endl;
					cout << endl;
					cout << "  FORAM CARREGADOS " << retLoc << " NOVOS LOCAIS NA APLICACAO" << endl;
				}
}

void runMenuFxLoc()
{
	string str="";
	int ret;

	do
	{
		system("CLS");
		cabecalho();
		cout << "                         MENU CARREGAR FICHEIRO DE LOCAIS                      " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA LOCALIZACAO                                                           " << endl;
		rodape();

		getline(cin,str);
		cin.clear();
		trim(str);
	}
	while (str=="");

	ret=t.lerFxLocais(str);

	if(ret==-1)
	{
		system("CLS");
		cabecalho();
		cout << "                         MENU CARREGAR FICHEIRO DE LOCAIS                      " << endl;
		cout << "                                                                               " << endl;
		cout << "  FICHEIRO " << str << ".txt NAO ENCONTRADO!" << endl;
		rodape();
	}
	else
	{
		system("CLS");
		cabecalho();
		cout << "                         MENU CARREGAR FICHEIRO DE LOCAIS                      " << endl;
		cout << "                                                                               " << endl;
		cout << "  FICHEIRO " << str << ".txt ENCONTRADO!" << endl;
		cout << "  FORAM CARREGADOS " << ret << " NOVOS LOCAIS NA APLICACAO" << endl;
		rodape();

		if (ret > 0)
		{
			for (int i = 0; i < t.getActLoc(); i++)
				m.addGraphVertex(t.getVecLocais()[i]);
		}
	}
}

void runMenuFxVia()
{
	if(t.getActLoc()>1)
	{
		string str="";
		int ret;

		do
		{
			system("CLS");
			cabecalho();
			cout << "                     MENU CARREGAR FICHEIRO DE VIAS DE LIGACAO                 " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA LOCALIZACAO                                                           " << endl;
			rodape();
		
			getline(cin,str);
			cin.clear();
			trim(str);
		}
		while (str=="");

		ret=t.lerFxVias(str);

		if(ret==-1)
		{
			system("CLS");
			cabecalho();
			cout << "                     MENU CARREGAR FICHEIRO DE VIAS DE LIGACAO                 " << endl;
			cout << "                                                                               " << endl;
			cout << "  FICHEIRO " << str << ".txt NAO ENCONTRADO!" << endl;
			rodape();
		}
		else
		{
			system("CLS");
			cabecalho();
			cout << "                     MENU CARREGAR FICHEIRO DE VIAS DE LIGACAO                 " << endl;
			cout << "                                                                               " << endl;
			cout << "  FICHEIRO " << str << ".txt ENCONTRADO!" << endl;
			cout << "  FORAM CARREGADAS " << ret << " NOVAS VIAS DE LIGACAO NA APLICACAO" << endl;
			rodape();

			if (ret > 0)
			{
				for (int i = 0; i<t.getActVia(); i++)
					if (!m.existeRamo(t.getVecVias()[i], t.getVecVias()[i]->getOrigem(), t.getVecVias()[i]->getDestino()))
					{
						m.addGraphEdge(PViaLigacao(*((t.getVecVias())[i])), t.getVecVias()[i]->getOrigem(), t.getVecVias()[i]->getDestino());
						m.addGraphEdge(PViaLigacao(*((t.getVecVias())[i])), t.getVecVias()[i]->getDestino(), t.getVecVias()[i]->getOrigem());
					}
			}
		}
	}

	else
	{
		system("CLS");
		cabecalho();
		cout << "                     MENU CARREGAR FICHEIRO DE VIAS DE LIGACAO                 " << endl;
		cout << "                                                                               " << endl;
		cout << "  => ERRO! DEVEM EXISTIR PELO MENOS DOIS LOCAIS NA APLICACAO                   " << endl;
		rodape();
	}
}

void menuInsLocNat()
{
	string desc="";
	int area;

	do
	{
		system("CLS");
		cabecalho();
		cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA DESCRICAO                                                             " << endl;
		rodape();

		getline(cin,desc);
		cin.clear();
		trim(desc);
	}
	while (desc=="");

	transform(desc.begin(), desc.end(), desc.begin(), toupper);

	if(t.encontrarLocal(desc)==NULL)
	{
		do
		{
			system("CLS");
			cabecalho();
			cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA AREA (km2)                                                            " << endl;
			rodape();
			area=lerOpcao();
		}
		while (area < 0);

		Natural ln(desc, area);
		t.inserirLocal(&ln);
		m.addGraphVertex(t.encontrarLocal(desc));

		system("CLS");
		cabecalho();
		cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
		cout << "                                                                               " << endl;
		cout << "  LOCAL INSERIDO COM SUCESSO!" << endl;
		rodape();
	}
	else
	{
		system("CLS");
		cabecalho();
		cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
		cout << "                                                                               " << endl;
		cout << "  !!! ERRO !!!" << endl;
		cout << endl;
		cout << "  -> LOCAL " << desc << " JA EXISTENTE NA APLICACAO!" << endl;
		rodape();
	}
}

void menuInsLocHistCult()
{
	string desc="";
	int horaTmpMedVisita;
	int minTmpMedVisita;
	int horaAbertura;
	int minAbertura;
	int horaEncerramento;
	int minEncerramento;

	do
	{
		system("CLS");
		cabecalho();
		cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA DESCRICAO                                                             " << endl;
		rodape();

		getline(cin,desc);
		cin.clear();
		trim(desc);
	}
	while (desc=="");

	transform(desc.begin(), desc.end(), desc.begin(), toupper);

	if(t.encontrarLocal(desc)==NULL)
	{
		do
		{
			system("CLS");
			cabecalho();
			cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA TEMPO MEDIO DE VISITA (horas)                                         " << endl;
			rodape();
			horaTmpMedVisita=lerOpcao();
		}
		while (horaTmpMedVisita < 0 || horaTmpMedVisita > 23);

		do
		{
			system("CLS");
			cabecalho();
			cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA TEMPO MEDIO DE VISITA (minutos)                                       " << endl;
			rodape();
			minTmpMedVisita=lerOpcao();
		}
		while (minTmpMedVisita < 0 || minTmpMedVisita > 59);

		do
		{
			system("CLS");
			cabecalho();
			cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA HORARIO DE ABERTURA (horas)                                           " << endl;
			rodape();
			horaAbertura=lerOpcao();
		}
		while (horaAbertura < 0 || horaAbertura > 23);

		do
		{
			system("CLS");
			cabecalho();
			cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA HORARIO DE ABERTURA (minutos)                                         " << endl;
			rodape();
			minAbertura=lerOpcao();
		}
		while (minAbertura < 0 || minAbertura > 59);

		do
		{
			system("CLS");
			cabecalho();
			cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA HORARIO DE ENCERRAMENTO (horas)                                       " << endl;
			rodape();
			horaEncerramento=lerOpcao();
		}
		while (horaEncerramento < 0 || horaEncerramento > 23);

		do
		{
			system("CLS");
			cabecalho();
			cout << "         MENU INSERIR LOCAL DE INTERESSE TURISTICO HISTORICO E CULTURAL        " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA HORARIO DE ENCERRAMENTO (minutos)                                     " << endl;
			rodape();
			minEncerramento=lerOpcao();
		}
		while (minEncerramento < 0 || minEncerramento > 59);

		if ((horaAbertura*60+minAbertura)<(horaEncerramento*60+minEncerramento))
		{
			if((horaTmpMedVisita*60+minTmpMedVisita)<=((horaEncerramento*60+minEncerramento)-(horaAbertura*60+minAbertura)))
			{
				HistoricoCultural lhc(desc, (horaTmpMedVisita*60+minTmpMedVisita), (horaAbertura*60+minAbertura), (horaEncerramento*60+minEncerramento));
				t.inserirLocal(&lhc);
				m.addGraphVertex(t.encontrarLocal(desc));

				system("CLS");
				cabecalho();
				cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
				cout << "                                                                               " << endl;
				cout << "  LOCAL INSERIDO COM SUCESSO!" << endl;
				rodape();
			}
			else
			{
				system("CLS");
				cabecalho();
				cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
				cout << "                                                                               " << endl;
				cout << "  !!! ERRO !!!" << endl;
				cout << endl;
				cout << "  -> TEMPO MEDIO DE VISITA SUPERIOR AO TEMPO EM QUE O LOCAL SE ENCONTRA ABERTO!" << endl;
				rodape();
			}
		}
		else
		{
			if ((horaAbertura*60+minAbertura)==(horaEncerramento*60+minEncerramento))
			{
				system("CLS");
				cabecalho();
				cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
				cout << "                                                                               " << endl;
				cout << "  !!! ERRO !!!" << endl;
				cout << endl;
				cout << "  -> HORARIO DE ABERTURA IGUAL AO HORARIO DE ENCERRAMENTO!" << endl;
				rodape();
			}
			else
			{
				system("CLS");
				cabecalho();
				cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
				cout << "                                                                               " << endl;
				cout << "  !!! ERRO !!!" << endl;
				cout << endl;
				cout << "  -> HORARIO DE ABERTURA POSTERIOR AO HORARIO DE ENCERRAMENTO!" << endl;
				rodape();
			}
		}
	}
	else
	{
		system("CLS");
		cabecalho();
		cout << "               MENU INSERIR LOCAL DE INTERESSE TURISTICO NATURAL               " << endl;
		cout << "                                                                               " << endl;
		cout << "  !!! ERRO !!!" << endl;
		cout << endl;
		cout << "  -> LOCAL " << desc << " JA EXISTENTE NA APLICACAO!" << endl;
		rodape();
	}
}

void menuInsViaEN()
{
	string origem="";
	string destino="";
	string codVia="";
	int totalKms;
	int horaTmpMedPerc;
	int minTmpMedPerc;
	string pavimento;

	do
	{
		system("CLS");
		cabecalho();
		cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA LOCAL DE ORIGEM                                                       " << endl;
		rodape();

		getline(cin,origem);
		cin.clear();
		trim(origem);
	}
	while (origem=="");

	transform(origem.begin(), origem.end(), origem.begin(), toupper);

	if(t.encontrarLocal(origem)!=NULL)
	{
		do
		{
			system("CLS");
			cabecalho();
			cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA LOCAL DE DESTINO                                                      " << endl;
			rodape();

			getline(cin,destino);
			cin.clear();
			trim(destino);
		}
		while (destino=="");

		transform(destino.begin(), destino.end(), destino.begin(), toupper);

		if(t.encontrarLocal(destino)!=NULL)
		{
			regex nacional("EN[1-9][[:digit:]]{0,2}(-[1-9])?");
			do
			{
				system("CLS");
				cabecalho();
				cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
				cout << "                                                                               " << endl;
				cout << "  INSIRA CODIGO DA VIA                                                         " << endl;
				rodape();

				getline(cin,codVia);
				cin.clear();
				trim(codVia);

				transform(codVia.begin(), codVia.end(), codVia.begin(), toupper);
			}
			while(!regex_match(codVia,nacional));

			if((t.encontrarVia(origem, destino, codVia))==NULL)
			{
				do
				{
					system("CLS");
					cabecalho();
					cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TOTAL DE KMS DA VIA                                                   " << endl;
					rodape();
					totalKms=lerOpcao();
				}
				while (totalKms<=0);

				do
				{
					system("CLS");
					cabecalho();
					cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TEMPO MEDIO DO PERCURSO (horas)                                       " << endl;
					rodape();
					horaTmpMedPerc=lerOpcao();
				}
				while (horaTmpMedPerc<0);

				do
				{
					system("CLS");
					cabecalho();
					cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TEMPO MEDIO DO PERCURSO (minutos)                                     " << endl;
					rodape();
					minTmpMedPerc=lerOpcao();
				}
				while (minTmpMedPerc < 0 || minTmpMedPerc > 59);

				do
				{
					system("CLS");
					cabecalho();
					cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TIPO DE PAVIMENTO                                                     " << endl;
					rodape();

					getline(cin,pavimento);
					cin.clear();
					trim(pavimento);
				}
				while (pavimento=="");

				transform(pavimento.begin(), pavimento.end(), pavimento.begin(), toupper);

				EN estNac(t.encontrarLocal(origem),t.encontrarLocal(destino), codVia, totalKms, (horaTmpMedPerc*60+minTmpMedPerc), pavimento);
				t.inserirVia(&estNac);
				m.addGraphEdge(PViaLigacao(estNac), estNac.getOrigem(), estNac.getDestino());
				m.addGraphEdge(PViaLigacao(estNac), estNac.getDestino(), estNac.getOrigem());

				system("CLS");
				cabecalho();
				cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
				cout << "                                                                               " << endl;
				cout << "  VIA DE LIGACAO INSERIDA COM SUCESSO!" << endl;
				rodape();
			}
			else
			{
				system("CLS");
				cabecalho();
				cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
				cout << "                                                                               " << endl;
				cout << "  !!! ERRO !!!" << endl;
				cout << endl;
				cout << "  -> ESTRADA NACIONAL " << codVia << endl;
				cout << "  -> ENTRE " << origem << " E " << destino << endl;
				cout << "  -> JA EXISTENTE NA APLICACAO!" << endl;
				rodape();
			}
		}
		else
		{
			system("CLS");
			cabecalho();
			cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
			cout << "                                                                               " << endl;
			cout << "  !!! ERRO !!!" << endl;
			cout << endl;
			cout << "  -> LOCAL " << destino << " NAO EXISTENTE NA APLICACAO!" << endl;
			rodape();
		}
	}
	else
	{
		system("CLS");
		cabecalho();
		cout << "                 MENU INSERIR VIA DE LIGACAO - ESTRADA NACIONAL                " << endl;
		cout << "                                                                               " << endl;
		cout << "  !!! ERRO !!!" << endl;
		cout << endl;
		cout << "  -> LOCAL " << origem << " NAO EXISTENTE NA APLICACAO!" << endl;
		rodape();
	}
}

void menuInsViaAE()
{
	string origem="";
	string destino="";
	string codVia="";
	int totalKms;
	int horaTmpMedPerc;
	int minTmpMedPerc;
	string portagem;

	do
	{
		system("CLS");
		cabecalho();
		cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA LOCAL DE ORIGEM                                                       " << endl;
		rodape();

		getline(cin,origem);
		cin.clear();
		trim(origem);
	}
	while (origem=="");

	transform(origem.begin(), origem.end(), origem.begin(), toupper);

	if(t.encontrarLocal(origem)!=NULL)
	{
		do
		{
			system("CLS");
			cabecalho();
			cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA LOCAL DE DESTINO                                                      " << endl;
			rodape();

			getline(cin,destino);
			cin.clear();
			trim(destino);
		}
		while (destino=="");

		transform(destino.begin(), destino.end(), destino.begin(), toupper);

		if(t.encontrarLocal(destino)!=NULL)
		{
			regex autoestrada("A[1-9][[:digit:]]?");
			do
			{
				system("CLS");
				cabecalho();
				cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
				cout << "                                                                               " << endl;
				cout << "  INSIRA CODIGO DA VIA                                                         " << endl;
				rodape();

				getline(cin,codVia);
				cin.clear();
				trim(codVia);

				transform(codVia.begin(), codVia.end(), codVia.begin(), toupper);
			}
			while(!regex_match(codVia,autoestrada));

			if((t.encontrarVia(origem, destino, codVia))==NULL)
			{
				do
				{
					system("CLS");
					cabecalho();
					cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TOTAL DE KMS DA VIA                                                   " << endl;
					rodape();
					totalKms=lerOpcao();
				}
				while (totalKms<=0);

				do
				{
					system("CLS");
					cabecalho();
					cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TEMPO MEDIO DO PERCURSO (horas)                                       " << endl;
					rodape();
					horaTmpMedPerc=lerOpcao();
				}
				while (horaTmpMedPerc<0);

				do
				{
					system("CLS");
					cabecalho();
					cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA TEMPO MEDIO DO PERCURSO (minutos)                                     " << endl;
					rodape();
					minTmpMedPerc=lerOpcao();
				}
				while (minTmpMedPerc < 0 || minTmpMedPerc > 59);

				regex port("([0-9]|([1-9][0-9]*))(\\.[0-9][0-9]?)?");

				do
				{
					system("CLS");
					cabecalho();
					cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
					cout << "                                                                               " << endl;
					cout << "  INSIRA PRECO DAS PORTAGENS                                                   " << endl;
					rodape();

					getline(cin,portagem);
					cin.clear();
					trim(portagem);
				}
				while (!regex_match(portagem,port));

				AE autEst(t.encontrarLocal(origem),t.encontrarLocal(destino), codVia, totalKms, (horaTmpMedPerc*60+minTmpMedPerc), stof(portagem));
				t.inserirVia(&autEst);
				m.addGraphEdge(PViaLigacao(autEst), autEst.getOrigem(), autEst.getDestino());
				m.addGraphEdge(PViaLigacao(autEst), autEst.getDestino(), autEst.getOrigem());

				system("CLS");
				cabecalho();
				cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
				cout << "                                                                               " << endl;
				cout << "  VIA DE LIGACAO INSERIDA COM SUCESSO!" << endl;
				rodape();
			}
			else
			{
				system("CLS");
				cabecalho();
				cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
				cout << "                                                                               " << endl;
				cout << "  !!! ERRO !!!" << endl;
				cout << endl;
				cout << "  -> AUTO-ESTRADA " << codVia << endl;
				cout << "  -> ENTRE " << origem << " E " << destino << endl;
				cout << "  -> JA EXISTENTE NA APLICACAO!" << endl;
				rodape();
			}
		}
		else
		{
			system("CLS");
			cabecalho();
			cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
			cout << "                                                                               " << endl;
			cout << "  !!! ERRO !!!" << endl;
			cout << endl;
			cout << "  -> LOCAL " << destino << " NAO EXISTENTE NA APLICACAO!" << endl;
			rodape();
		}
	}
	else
	{
		system("CLS");
		cabecalho();
		cout << "                   MENU INSERIR VIA DE LIGACAO - AUTO-ESTRADA                  " << endl;
		cout << "                                                                               " << endl;
		cout << "  !!! ERRO !!!" << endl;
		cout << endl;
		cout << "  -> LOCAL " << origem << " NAO EXISTENTE NA APLICACAO!" << endl;
		rodape();
	}
}

void runMenuTemas()
{
	int opcao;

	do
	{
		system("CLS");
		cabecalho();
		menuTemas();
		rodape();
		opcao = lerOpcao();

		switch (opcao)
		{
			case 1:
			{
				system("Color 07");
				break;
			}

			case 2:
			{
				system("Color F0");
				break;
			}

			case 3:
			{
				system("Color 1F");
				break;
			}

			case 4:
			{
				system("Color 4F");
				break;
			}

			case 5:
			{
				system("Color 2F");
				break;
			}

			case 0:
			{
				break;
			}

			default:
			{
				break;
			}
		}

	} while (opcao != 0);
}

void menuInsOrigemDestino(int& opcao)
{
	string origem = "";
	string destino = "";

	do
	{
		system("CLS");
		cabecalho();
		if (opcao == 10)
			cout << "           MENU LISTAR TODOS OS PERCURSOS POSSIVEIS ENTRE DOIS LOCAIS          " << endl;
		if (opcao == 11)
			cout << "           MENU CALCULAR PERCURSO MAIS CURTO EM KMS ENTRE DOIS LOCAIS          " << endl;
		if (opcao == 12)
			cout << "         MENU CALCULAR PERCURSO MAIS RAPIDO EM TEMPO ENTRE DOIS LOCAIS         " << endl;
		if (opcao == 13)
			cout << "        MENU CALCULAR PERCURSO MAIS ECONOMICO EM EUROS ENTRE DOIS LOCAIS       " << endl;
		if (opcao == 14)
			cout << "     MENU CALCULAR PERCURSO DE MAIOR INTERESSE TURISTICO ENTRE DOIS LOCAIS     " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA LOCAL DE ORIGEM                                                       " << endl;
		rodape();

		getline(cin, origem);
		cin.clear();
		trim(origem);
	} while (origem == "");

	transform(origem.begin(), origem.end(), origem.begin(), toupper);

	if (m.existeVertice(origem))
	{
		do
		{
			system("CLS");
			cabecalho();
			if (opcao == 10)
				cout << "           MENU LISTAR TODOS OS PERCURSOS POSSIVEIS ENTRE DOIS LOCAIS          " << endl;
			if (opcao == 11)
				cout << "           MENU CALCULAR PERCURSO MAIS CURTO EM KMS ENTRE DOIS LOCAIS          " << endl;
			if (opcao == 12)
				cout << "         MENU CALCULAR PERCURSO MAIS RAPIDO EM TEMPO ENTRE DOIS LOCAIS         " << endl;
			if (opcao == 13)
				cout << "        MENU CALCULAR PERCURSO MAIS ECONOMICO EM EUROS ENTRE DOIS LOCAIS       " << endl;
			if (opcao == 14)
				cout << "     MENU CALCULAR PERCURSO DE MAIOR INTERESSE TURISTICO ENTRE DOIS LOCAIS     " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA LOCAL DE DESTINO                                                      " << endl;
			rodape();

			getline(cin, destino);
			cin.clear();
			trim(destino);
		} while (destino == "");

		transform(destino.begin(), destino.end(), destino.begin(), toupper);

		if (m.existeVertice(destino))
		{
				system("CLS");
				cabecalho();
				if (opcao == 10)
					if (!m.percursosPossiveis(origem, destino))
					{
						cout << "           MENU LISTAR TODOS OS PERCURSOS POSSIVEIS ENTRE DOIS LOCAIS          " << endl;
						cout << "                                                                               " << endl;
						cout << "  NAO EXISTE NENHUM PERCURSO POSSIVEL ENTRE                                    " << endl;
						cout << "  -> " << origem << " E " << destino << endl;
					}
				if (opcao == 11)
					if (m.existeCaminho(origem, destino))
						m.menorCaminhoDistancia(origem, destino);
					else
						{
							cout << "           MENU CALCULAR PERCURSO MAIS CURTO EM KMS ENTRE DOIS LOCAIS          " << endl;
							cout << "                                                                               " << endl;
							cout << "  NAO EXISTE NENHUM CAMINHO POSSIVEL ENTRE                                     " << endl;
							cout << "  -> " << origem << " E " << destino << endl;
						}
				if (opcao == 12)
					if (m.existeCaminho(origem, destino))
						m.menorCaminhoTempo(origem, destino);
					else
						{
							cout << "         MENU CALCULAR PERCURSO MAIS RAPIDO EM TEMPO ENTRE DOIS LOCAIS         " << endl;
							cout << "                                                                               " << endl;
							cout << "  NAO EXISTE NENHUM CAMINHO POSSIVEL ENTRE                                     " << endl;
							cout << "  -> " << origem << " E " << destino << endl;
						}
				if (opcao == 13)
					if (m.existeCaminho(origem, destino))
						m.menorCaminhoCusto(origem, destino);
					else
						{
							cout << "        MENU CALCULAR PERCURSO MAIS ECONOMICO EM EUROS ENTRE DOIS LOCAIS       " << endl;
							cout << "                                                                               " << endl;
							cout << "  NAO EXISTE NENHUM CAMINHO POSSIVEL ENTRE                                     " << endl;
							cout << "  -> " << origem << " E " << destino << endl;
						}
				if (opcao == 14)
				if (!m.caminhoMaiorInteresseTuristico(origem, destino))
					{
						cout << "     MENU CALCULAR PERCURSO DE MAIOR INTERESSE TURISTICO ENTRE DOIS LOCAIS     " << endl;
						cout << "                                                                               " << endl;
						cout << "  NAO EXISTE NENHUM PERCURSO POSSIVEL ENTRE                                    " << endl;
						cout << "  -> " << origem << " E " << destino << endl;
					}
				rodape();
		}
		else
		{
			system("CLS");
			cabecalho();
			if (opcao == 10)
				cout << "           MENU LISTAR TODOS OS PERCURSOS POSSIVEIS ENTRE DOIS LOCAIS          " << endl;
			if (opcao == 11)
				cout << "           MENU CALCULAR PERCURSO MAIS CURTO EM KMS ENTRE DOIS LOCAIS          " << endl;
			if (opcao == 12)
				cout << "         MENU CALCULAR PERCURSO MAIS RAPIDO EM TEMPO ENTRE DOIS LOCAIS         " << endl;
			if (opcao == 13)
				cout << "        MENU CALCULAR PERCURSO MAIS ECONOMICO EM EUROS ENTRE DOIS LOCAIS       " << endl;
			if (opcao == 14)
				cout << "     MENU CALCULAR PERCURSO DE MAIOR INTERESSE TURISTICO ENTRE DOIS LOCAIS     " << endl;
			cout << "                                                                               " << endl;
			cout << "  !!! ERRO !!!" << endl;
			cout << endl;
			cout << "  -> LOCAL " << destino << " NAO EXISTENTE NA APLICACAO!" << endl;
			rodape();
		}
	}
	else
	{
		system("CLS");
		cabecalho();
		if (opcao == 10)
			cout << "           MENU LISTAR TODOS OS PERCURSOS POSSIVEIS ENTRE DOIS LOCAIS          " << endl;
		if (opcao == 11)
			cout << "           MENU CALCULAR PERCURSO MAIS CURTO EM KMS ENTRE DOIS LOCAIS          " << endl;
		if (opcao == 12)
			cout << "         MENU CALCULAR PERCURSO MAIS RAPIDO EM TEMPO ENTRE DOIS LOCAIS         " << endl;
		if (opcao == 13)
			cout << "        MENU CALCULAR PERCURSO MAIS ECONOMICO EM EUROS ENTRE DOIS LOCAIS       " << endl;
		if (opcao == 14)
			cout << "     MENU CALCULAR PERCURSO DE MAIOR INTERESSE TURISTICO ENTRE DOIS LOCAIS     " << endl;
		cout << "                                                                               " << endl;
		cout << "  !!! ERRO !!!" << endl;
		cout << endl;
		cout << "  -> LOCAL " << origem << " NAO EXISTENTE NA APLICACAO!" << endl;
		rodape();
	}
}

void menuTemas()
{
	cout << "                               TEMA DA APLICACAO                               " << endl;
	cout << "                                                                               " << endl;
	cout << "  1 - Tema Default                                                             " << endl;
	cout << "                                                                               " << endl;
	cout << "  2 - Tema Reverse                                                             " << endl;
	cout << "                                                                               " << endl;
	cout << "  3 - Tema FC PORTO                                                            " << endl;
	cout << "                                                                               " << endl;
	cout << "  4 - Tema SL BENFICA                                                          " << endl;
	cout << "                                                                               " << endl;
	cout << "  5 - Tema SPORTING CP                                                         " << endl;
	cout << "                                                                               " << endl;
	cout << "  0 - Voltar                                                                   " << endl;
}



void menuItinerario()
{
	list<Local*> locais;
	string local = "";
	Local* locInicio;
	Local* loc = NULL;
	int horaInicio;
	int minInicio;
	int qtdLocais;
	bool flag = false;

	do
	{
		system("CLS");
		cabecalho();
		cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
		cout << "                                                                               " << endl;
		cout << "  INSIRA LOCAL INICIAL DA VIAGEM                                               " << endl;
		rodape();

		getline(cin, local);
		cin.clear();
		trim(local);
	} while (local == "");

	transform(local.begin(), local.end(), local.begin(), toupper);

	locInicio = t.encontrarLocal(local);

	if (locInicio != NULL)
	{
		do
		{
			system("CLS");
			cabecalho();
			cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA HORARIO INICIAL DA VIAGEM (horas)                                     " << endl;
			rodape();
			horaInicio = lerOpcao();
		} while (horaInicio < 0 || horaInicio > 23);

		do
		{
			system("CLS");
			cabecalho();
			cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
			cout << "                                                                               " << endl;
			cout << "  INSIRA HORARIO INICIAL DA VIAGEM (minutos)                                   " << endl;
			rodape();
			minInicio = lerOpcao();
		} while (minInicio < 0 || minInicio > 59);

		do
		{
			system("CLS");
			cabecalho();
			cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
			cout << "                                                                               " << endl;
			cout << "  EXISTEM " << t.getActLoc() << " LOCAIS DIFERENTES PARA VISITAR" << endl;
			cout << "                                                                               " << endl;
			cout << "  QUANTOS LOCAIS PRETENDE VISITAR?                                             " << endl;
			rodape();
			qtdLocais = lerOpcao();
		} while (qtdLocais < 0 || qtdLocais > t.getActLoc());

		if (qtdLocais > 0)
		{
			int i = 0;
			bool localRepetido = false;
			do
			{
				do
				{
					system("CLS");
					cabecalho();
					cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
					cout << "                                                                               " << endl;
					if (localRepetido == false)
						cout << "  INSIRA O " << i + 1 << "o LOCAL A VISITAR" << endl;
					else
					{
						cout << "  !!! ERRO !!!" << endl;
						cout << endl;
						cout << "  -> " << loc->getDesc() << endl;
						cout << "  -> LOCAL JA EXISTENTE NA LISTA DE LOCAIS A VISITAR                       " << endl;
						cout << endl;
						cout << "  INSIRA NOVAMENTE O " << i + 1 << "o LOCAL A VISITAR" << endl;
					}
					rodape();

					getline(cin, local);
					cin.clear();
					trim(local);
				} while (local == "");

				localRepetido = false;

				transform(local.begin(), local.end(), local.begin(), toupper);

				loc = t.encontrarLocal(local);

				if (loc != NULL)
				{
					if (i == 0)
					{
						if (loc == locInicio)
						{

							locais.push_back(loc);
							i++;
						}
						else
						{
							if (m.existeCaminho(locInicio->getDesc(), loc->getDesc()))
							{
								locais.push_back(loc);
								i++;
							}
							else
							{
								flag = true;
								system("CLS");
								cabecalho();
								cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
								cout << "                                                                               " << endl;
								cout << "  !!! ERRO !!!" << endl;
								cout << endl;
								cout << "  NAO EXISTE NENHUM CAMINHO POSSIVEL ENTRE                                     " << endl;
								cout << "  -> " << locInicio->getDesc() << " E " << loc->getDesc() << endl;
								rodape();
							}
						}
					}
					else
					{
						for (list<Local*>::iterator it = locais.begin(); it != locais.end(); it++)
							if (*(it) == loc)
								localRepetido = true;
						if (localRepetido == false)
						{
							if (m.existeCaminho(locais.front()->getDesc(), loc->getDesc()))
							{
								locais.push_back(loc);
								i++;
							}
							else
							{
								flag = true;
								system("CLS");
								cabecalho();
								cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
								cout << "                                                                               " << endl;
								cout << "  !!! ERRO !!!" << endl;
								cout << endl;
								cout << "  NAO EXISTE NENHUM CAMINHO POSSIVEL ENTRE                                     " << endl;
								cout << "  -> " << locais.front()->getDesc() << " E " << loc->getDesc() << endl;
								rodape();
							}
						}
					}
				}
				else
				{
					flag = true;
					system("CLS");
					cabecalho();
					cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
					cout << "                                                                               " << endl;
					cout << "  !!! ERRO !!!" << endl;
					cout << endl;
					cout << "  -> LOCAL " << local << " NAO EXISTENTE NA APLICACAO!" << endl;
					rodape();
				}
			} while (i < qtdLocais && flag == false);

			if (i == qtdLocais && flag == false)
			{
				int opcao;
				do
				{
					system("CLS");
					cabecalho();
					cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
					cout << "                                                                               " << endl;
					cout << "  ESCOLHA TIPO DE LIGACAO ENTRE OS LOCAIS DO ITINERARIO                        " << endl;
					cout << "                                                                               " << endl;
					cout << "  1 - Ligacao mais curta em KMS entre cada local                               " << endl;
					cout << "                                                                               " << endl;
					cout << "  2 - Ligacao mais rapida em TEMPO entre cada local                            " << endl;
					cout << "                                                                               " << endl;
					cout << "  3 - Ligacao mais economica em EUROS entre cada local                         " << endl;
					rodape();

					opcao = lerOpcao();
				} while (opcao < 1 || opcao > 3);

				stack<Local*> s;
				for (list<Local*>::iterator it = locais.begin(); it != locais.end(); it++)
					s.push(*it);
				system("CLS");
				cabecalho();
				m.itinerario(horaInicio * 60 + minInicio, locInicio, s, opcao);
				rodape();
			}
		}
		else
		{
			system("CLS");
			cabecalho();
			cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
			cout << "                                                                               " << endl;
			cout << "  !!! ERRO !!!" << endl;
			cout << endl;
			cout << "  -> NENHUM LOCAL A VISITAR!                                                   " << endl;
			rodape();
		}

	}
	else
	{
		system("CLS");
		cabecalho();
		cout << "           MENU CRIAR ITINERARIO E CALCULAR INSTANTE FINAL DA VIAGEM           " << endl;
		cout << "                                                                               " << endl;
		cout << "  !!! ERRO !!!" << endl;
		cout << endl;
		cout << "  -> LOCAL " << local << " NAO EXISTENTE NA APLICACAO!" << endl;
		rodape();
	}
}

void trim(string& str)
{
	string trim_chars = " \t";

	string::size_type pos = str.find_last_not_of(trim_chars);

	if(pos != string::npos)
	{
		str.erase(pos + 1);
		pos = str.find_first_not_of(trim_chars);
		if(pos != string::npos)
			str.erase(0, pos);
	}
	else
		str.erase(str.begin(), str.end());
}
