#ifndef Natural_
#define Natural_

#include <iostream>
#include <string>

using namespace std;

#include "Local.h"

class Natural : public Local
{
private:
	int area;

public:
	Natural();
	Natural(string descricao, int a);
	Natural(const Natural& n);
	Local* clone() const;
	~Natural();

	int getArea() const;
	void setArea(int a);

	void listar() const;

	Natural& operator=(const Natural& n);
	void escrever(ostream& out) const;
};

Natural::Natural():Local()
{
	area = 0;
}

Natural::Natural(string descricao, int a):Local(descricao)
{
	area = a;
}

Natural::Natural(const Natural& n):Local(n)
{
	area = n.area;
}

Local* Natural::clone() const
{
	return new Natural(*this);
}

Natural::~Natural()
{

}

int Natural::getArea() const 
{
	return area;
}

void Natural::setArea(int a) 
{
	area = a;
}

void Natural::listar() const
{
	cout << "LOCAL DE INTERESSE TURISTICO NATURAL" << endl;
	Local::listar();
	cout << "AREA: " << area << " km2" << endl;
	cout << endl;
}

Natural& Natural::operator=(const Natural& n)
{
	if(this!=&n)
	{
		area=n.area;
		Local::operator=(n);
	}
	return (*this);
}

void Natural::escrever(ostream& out) const
{
	out << "LOCAL DE INTERESSE TURISTICO NATURAL" << endl;
	Local::escrever(cout);
	out << "AREA: " << area << " km2" << endl;
}

#endif
