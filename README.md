![ISEP](http://www.dei.isep.ipp.pt/images/topo_index.png "LEI-ISEP")
# ESINF - Estruturas de Informação #
###### Trabalho Prático - Ano Lectivo 2013/2014 ######


    1050840 - Jose Vieira
    1090533 - Andre Sousa
* * *
## MAPA DIGITAL ##
Pretende-se com este trabalho especificar e desenvolver um **Mapa Digital** com informação associada a locais de interesse turístico bem como as vias de ligação entre os mesmos. Os locais de interesse turístico são lugares que os turistas visitam, geralmente pelo seu valor cultural, importância histórica, beleza natural ou artificial, originalidade, raridade, ou ainda para a prática de atividades recreativas, desportivas ou de lazer.

Face à grande variedade e heterogeneidade de locais de interesse turístico estes serão divididos em locais de interesse turístico naturais, como parques/reservas naturais, praias, grutas, etc. e locais de interesse turístico históricos e culturais, onde se incluem museus, palácios, bibliotecas, etc. Todos os locais de interesse turístico são caracterizados por uma descrição. Aos locais históricos acresce o tempo médio de visita em minutos, o horário de abertura e o horário de encerramento (para facilitar, estes dois horários são medidos em minutos desde as 00:00 horas). Para os locais de interesse turístico naturais é guardada a respetiva área em km2.

No que respeita às vias de ligação entre os locais de interesse turístico a informação geral associada corresponde ao código da via, ao total de kms da via e ao tempo médio do percurso em minutos entre um local origem e um local destino. Existem vários tipos de vias de ligação, mas por questões de simplificação, consideram-se apenas autoestradas e estradas nacionais. Uma autoestrada guarda informação sobre o preço das portagens. As estradas nacionais possuem informação sobre o tipo de pavimento que possuem (asfalto, paralelo, terra batida, etc.).

Aproveitando as facilidades oferecidas pelo paradigma orientado a objetos, o trabalho deverá ser realizado de forma incremental — começando com as classes mais simples até se chegar ao sistema final. É importante testarem as diferentes classes à medida que as desenvolvem. Serão assim estabelecidos os seguintes objectivos intermédios a alcançar.
###### 1ª Parte - Cotação: 30% ######
Desenvolva as classes necessárias ao Mapa Digital acima especificado. Para cada classe deverão ser implementados os construtores, o destrutor, os métodos para actualização/acesso aos membros de dados privados, e a sobrecarga dos operadores necessários ao bom funcionamento da aplicação.

Desenvolva uma classe **Teste** que irá guardar informação relativa aos locais de interesse turístico e respetivas vias de ligação, em  contentores. A classe para além dos construtores e destrutor deverá também disponibilizar métodos para:

- Inserir os locais de interesse turístico a partir de um ficheiro de texto com a informação estruturada do seguinte modo:

######

    LocTurist1,9000
    LocTurist2,120,570,1110 //Tempo médio de visita 2h está aberto das 9:30h às 18:30h
    LocTurist3,5500
    LocTurist4,2300
    LocTurist5,20,540,870 //Tempo médio de visita 20 min está aberto das 9:00h às 14:30h


- Inserir as vias de ligação a partir de um ficheiro de texto com a informação estruturada do seguinte modo:

######

    LocTurist1,LocTurist2,EN1,100,90,asfalto
    LocTurist3,LocTurist7,EN2,55,35,paralelo
    LocTurist2,LocTurist5,EN1,200,150,asfalto
    …
    LocTurist5,LocTurist2,A3,54,30,3.25
    LocTurist4,LocTurist1,A1,120,60,13.25
    LocTurist9,LocTurist6,A4,80,90,7.05
    …
    LocTurist1,LocTurist7,A2,65,40,4.15

- Contabilizar os locais de interesse turístico históricos e naturais e apresentá-los por ordem alfabética da sua descrição.

###### 2ª Parte - Cotação: 70% ######
Pretende-se que implementem o **Mapa Digital** através de um grafo usando a classe `graphStl` e na qual a manipulação ocorra indistintamente ao nível dos locais de interesse turístico, históricos e naturais, bem como usando os dois tipos de vias: autoestradas e estradas nacionais.

A classe para além dos construtores e destrutor deverá disponibilizar funcionalidades para:

*   Construir o grafo a partir dos objetos anteriormente criados na classe Teste
*   Determine a complexidade temporal (notação Big-Oh) do método de construção do grafo.
*   Apresentar todos os percursos possíveis entre dois locais de interesse turístico
*   Calcular o percurso entre dois locais de interesse turístico:
    *   mais curto em Kms
    *   mais económico em Euros
    *   de maior interesse turístico (percurso que inclua o maior número de locais de interesse turístico)

**Nota:** Para cada percurso devem apresentar os nós que o constituem e as vias de ligação usadas.

- Dado um local de interesse turístico inicial, o instante de tempo de saída (também medido em minutos a partir das 0:00) e uma lista de pontos turísticos a visitar, calcular o instante em que acaba a visita. Se o turista chegar a um local de interesse turístico antes da sua abertura espera pela abertura. Se um turista chegar a um local de interesse turístico num instante que somado ao tempo recomendado de visita leva a que se ultrapasse a hora de fecho, então não visita esse local de interesse turístico.

A aplicação deve possuir uma interface simples que permita invocar todas as funcionalidades pedidas.

###### Normas ######

-   O trabalho deverá ser realizado em **grupos de dois alunos**.
-   É obrigatório o uso da ferramenta GIT de controlo de versões.
-   A entrega da 1ª Parte (até **28-Out-2013**) envolve submeter o projeto em código C++ elaborado.
-   A avaliação da 1ª Parte do Trabalho Prático será feita principalmente em função da conformidade da solução proposta com o Paradigma da POO.
-   O projeto entregue na 1ª Parte poderá ser corrigido na 2ª Parte de acordo com os comentários/sugestões do professor.
-   A entrega da 2ª Parte (até **8-Dez-2012**) envolve a submissão do projeto final e relatório.
-   O projeto será avaliado de acordo com as funcionalidades implementadas **modularidade, organização, clareza e eficiência do código**.
-   Cada parte do trabalho deverá ser submetida no moodle, na área da disciplina, até às **24 horas do dia indicado**. A partir das datas indicadas, a nota do trabalho será penalizada **10% por cada dia de atraso** e não se aceitam trabalhos após **dois dias** das datas indicadas.
-   O projeto criado em código C++, ficheiros de teste e relatório devem ser colocados num ficheiro ZIP, com a designação: **TPESINF-TURMA-NºAluno-NºAluno.zip** por exemplo, **TPESINF-2DJ-1101350-1101460.zip**
A apresentação/avaliação do trabalho final será individual, em datas a fixar com o professor das aulas PL, na semana de **9-14 Dezembro**.
